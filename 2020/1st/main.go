package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

// findMatchingSummation adds every element in a list against every
// other value to find the matching pair that adds up to the value
// argument and returns the indexes of them.
// This function does not handle multiple matches and will only return
// the first one.
func findMatchingSummation(sums []int, value int) (indexes [2]int) {

	// Loop over each element in the list
	for i, val := range sums {
		// Compare it to every following element in the list
		for j, compVal := range sums[i+1:] {
			// if the elements add up to the value then return the indexes
			if val+compVal == value {
				indexes = [2]int{i, i + 1 + j}
				return
			}
		}
	}
	return
}

// findMatchingTripleSummation adds every element in a list against every
// other value to find the matching pair that adds up to the value
// argument and returns the indexes of them.
// This function does not handle multiple matches and will only return
// the first one.
func findMatchingTripleSummation(sums []int, value int) (indexes [3]int) {

	// Loop over each element in the list
	for i, val := range sums {
		// Compare it to every following elements in the list
		for j, compVal := range sums[i+1:] {
			for k, secondCompVal := range sums[j+1:] {
				// if the elements add up to the value then return the indexes
				if val+compVal+secondCompVal == value {
					indexes = [3]int{i, i + 1 + j, j + 1 + k}
					return
				}
			}
		}
	}
	return
}

func fileToList(fileName string) []int {
	f, err := os.Open(fileName) // os.OpenFile has more options if you need them
	defer f.Close()
	if err != nil { // error checking is good practice
		// error *handling* is good practice.  log.Fatal sends the error
		// message to stderr and exits with a non-zero code.
		log.Fatal(err)
	}

	// os.File has no special buffering, it makes straight operating system
	// requests.  bufio.Reader does buffering and has several useful methods.
	var inputs []int
	rd := bufio.NewReader(f)
	for {

		line, _, err := rd.ReadLine()
		if err == io.EOF {
			break
		}

		// loop termination condition 2: some other error.
		// Errors happen, so check for them and do something with them.
		if err != nil {
			log.Fatal(err)
		}
		i, err := strconv.Atoi(string(line))
		inputs = append(inputs, i)
	}
	return inputs
}

func main() {
	inputs := fileToList("actual_data.txt")
	var indexes [2]int = findMatchingSummation(inputs, 2020)
	fmt.Println(inputs[indexes[0]] * inputs[indexes[1]])
	var tripleIndexes [3]int = findMatchingTripleSummation(inputs, 2020)
	fmt.Println(inputs[tripleIndexes[0]] * inputs[tripleIndexes[1]] * inputs[tripleIndexes[2]])

}
