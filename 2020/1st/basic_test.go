package main

import "testing"

func TestFewInputs(t *testing.T) {
	var inputs []int = []int{1721, 979, 366, 299, 675, 1456}
	a := findMatchingSummation(inputs, 2020)
	if a[0] != 0 || a[1] != 3 {
		t.Fatalf("findMatchingSummation failed with indexes of %d, %d", a[0], a[1])
	}
}
func TestPartOneInputs(t *testing.T) {
	inputs := fileToList("actual_data.txt")
	a := findMatchingSummation(inputs, 2020)
	if a[0] != 169 || a[1] != 178 {
		t.Fatalf("findMatchingSummation failed with indexes of %d, %d", a[0], a[1])
	}
}

func TestFewTripleInputs(t *testing.T) {
	var inputs []int = []int{1721, 979, 366, 299, 675, 1456}
	a := findMatchingTripleSummation(inputs, 2020)
	if inputs[a[0]] != 979 || inputs[a[1]] != 366 || inputs[a[2]] != 675 {
		t.Fatalf("findMatchingTripleSummation failed with indexes of %d, %d, %d", a[0], a[1], a[2])
	}
}
