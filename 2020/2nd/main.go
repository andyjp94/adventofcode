package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
)

type passwordPolicy struct {
	requiredRune rune
	indexes      [2]uint
}

// parsePasswordConfig parses the config file structure provided as part of the
// day 2 challenge and returns a policy that can be used to check the password
// is valid along with the password. If the config does not match the expected
// format it errors
func parsePasswordConfig(passwordConfig string) (passwordPolicy, string, error) {
	r, err := regexp.Compile(`(\d+)-(\d+)\s([a-zA-Z]):\s([a-zA-Z]+)$`)
	if err != nil {
		return passwordPolicy{}, "", err
	}
	policyComponents := r.FindStringSubmatch(passwordConfig)
	// if the policyComponents is nil the regexp did not match
	if policyComponents == nil {
		return passwordPolicy{}, "", errors.New("The provided password configuration is not in the correct format")
	}
	// No need to check for errors, the regexp only allows integers
	firstPos, _ := strconv.Atoi(policyComponents[1])
	secondPos, _ := strconv.Atoi(policyComponents[2])

	policy := passwordPolicy{
		indexes:      [2]uint{uint(firstPos - 1), uint(secondPos - 1)},
		requiredRune: rune(policyComponents[3][0]),
	}

	return policy, policyComponents[4], nil
}

// isValidPassword checks a password against it's policy
func isValidPassword(password string, policy passwordPolicy) bool {
	var matches uint
	for _, v := range policy.indexes {
		if rune(password[v]) == policy.requiredRune {
			matches++

		}
	}
	if matches == 1 {
		return true
	}
	return false
}

func fileToList(fileName string) []string {
	f, err := os.Open(fileName) // os.OpenFile has more options if you need them
	defer f.Close()
	if err != nil { // error checking is good practice
		// error *handling* is good practice.  log.Fatal sends the error
		// message to stderr and exits with a non-zero code.
		log.Fatal(err)
	}

	// os.File has no special buffering, it makes straight operating system
	// requests.  bufio.Reader does buffering and has several useful methods.
	var inputs []string
	rd := bufio.NewReader(f)
	for {

		line, _, err := rd.ReadLine()
		if err == io.EOF {
			break
		}

		// loop termination condition 2: some other error.
		// Errors happen, so check for them and do something with them.
		if err != nil {
			log.Fatal(err)
		}
		// i, err := strconv.Atoi(string(line))
		inputs = append(inputs, string(line))
	}
	return inputs
}

func main() {
	var numValidPasswords int
	inputs := fileToList("actual_data.txt")
	for _, v := range inputs {
		policy, password, err := parsePasswordConfig(v)
		if err != nil {
			panic(err)
		}
		if isValidPassword(password, policy) {
			numValidPasswords++
		}
	}
	fmt.Printf("The number of valid passwords is %d\n", numValidPasswords)
}
