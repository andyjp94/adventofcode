package main

import (
	"reflect"
	"testing"
)

func Test_parsePasswordConfig(t *testing.T) {
	type args struct {
		passwordConfig string
	}
	tests := []struct {
		name    string
		args    args
		want    passwordPolicy
		want1   string
		wantErr bool
	}{
		{
			name: "Test First provided information",
			args: args{passwordConfig: "1-3 a: abcde"},
			want: passwordPolicy{
				requiredRune: rune('a'),
				indexes:      [2]uint{0, 2},
			},
			want1:   "abcde",
			wantErr: false,
		},
		{
			name: "Test Second provided information",
			args: args{passwordConfig: "1-3 b: cdefg"},
			want: passwordPolicy{
				requiredRune: rune('b'),
				indexes:      [2]uint{0, 2},
			},
			want1:   "cdefg",
			wantErr: false,
		},
		{
			name: "Test Third provided information",
			args: args{passwordConfig: "2-9 c: ccccccccc"},
			want: passwordPolicy{
				requiredRune: rune('c'),
				indexes:      [2]uint{1, 8},
			},
			want1:   "ccccccccc",
			wantErr: false,
		},
		{
			name: "Test invalid string structure",
			args: args{passwordConfig: "2-9  ccccccccc"},
			want: passwordPolicy{
				requiredRune: 0,
				indexes:      [2]uint{},
			},
			want1:   "",
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := parsePasswordConfig(tt.args.passwordConfig)
			if (err != nil) != tt.wantErr {
				t.Errorf("parsePasswordConfig() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parsePasswordConfig() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("parsePasswordConfig() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func Test_isValidPassword(t *testing.T) {
	type args struct {
		password string
		policy   passwordPolicy
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "First provided test",
			args: args{
				password: "abcde",
				policy: passwordPolicy{
					requiredRune: rune('a'),
					indexes:      [2]uint{0, 2},
				},
			},
			want: true,
		},
		{
			name: "Second provided test",
			args: args{
				password: "cdefg",
				policy: passwordPolicy{
					requiredRune: rune('b'),
					indexes:      [2]uint{0, 2},
				},
			},
			want: false,
		},
		{
			name: "Third provided test",
			args: args{
				password: "ccccccccc",
				policy: passwordPolicy{
					requiredRune: rune('c'),
					indexes:      [2]uint{1, 8},
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isValidPassword(tt.args.password, tt.args.policy); got != tt.want {
				t.Errorf("isValidPassword() = %v, want %v", got, tt.want)
			}
		})
	}
}
