package main

import (
	"bufio"
	"strings"
	"testing"
)

func Test_findTrees(t *testing.T) {
	type args struct {
		scanner    *bufio.Scanner
		startIndex uint
		increment  increment
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Empty scanner",
			args: args{
				scanner:    bufio.NewScanner(strings.NewReader("")),
				startIndex: 0,
				increment:  increment{x: 2, y: 1},
			},
			want: 0,
		},
		{
			name: "Find One",
			args: args{
				scanner:    bufio.NewScanner(strings.NewReader("..#")),
				startIndex: 0,
				increment:  increment{x: 2, y: 1},
			},
			want: 1,
		},
		{
			name: "Multiple Rows",
			args: args{
				scanner:    bufio.NewScanner(strings.NewReader("..#\n.#.")),
				startIndex: 0,
				increment:  increment{x: 2, y: 1},
			},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findTrees(tt.args.scanner, tt.args.startIndex, tt.args.increment); got != tt.want {
				t.Errorf("findTrees() = %v, want %v", got, tt.want)
			}
		})
	}
}
