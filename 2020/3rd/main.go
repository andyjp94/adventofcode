package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

const (
	tree = rune('#')
)

// findTrees recursively searches a scanner at set intervals
func findTrees(scanner *bufio.Scanner, startIndex uint, inc increment) int {

	// scan new line, check if there is anything left in the scanner
	for i := 0; int(inc.y) > i; i++ {
		scanFull := scanner.Scan()
		if scanFull == false {
			return 0
		}
	}

	// Get the text
	text := scanner.Text()
	textLength := uint(len(text))
	newIndex := startIndex + inc.x
	// If the index is greater than the length of the text wrap around
	if newIndex >= textLength {
		newIndex -= textLength
	}
	// If a tree is found increment the return value and call the function for the next line
	if rune(text[newIndex]) == tree {
		return 1 + findTrees(scanner, newIndex, inc)
	}
	return findTrees(scanner, newIndex, inc)
}

type increment struct {
	x uint
	y uint
}

func main() {

	f, err := os.Open("actual_data.txt") // os.OpenFile has more options if you need them
	defer f.Close()
	if err != nil { // error checking is good practice
		// error *handling* is good practice.  log.Fatal sends the error
		// message to stderr and exits with a non-zero code.
		log.Fatal(err)
	}
	var total int = 1
	for _, v := range []increment{{x: 1, y: 1}, increment{x: 3, y: 1}, increment{x: 5, y: 1}, increment{x: 7, y: 1}, increment{x: 1, y: 2}} {
		f.Seek(0, 0)
		scanner := bufio.NewScanner(f)
		scanner.Scan()

		total *= findTrees(scanner, 0, v)
	}
	fmt.Println(total)

}
