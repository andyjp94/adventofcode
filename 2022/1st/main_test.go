package main

import (
	"reflect"
	"testing"
)

func Test_findTopCalories(t *testing.T) {
	type args struct {
		elves    string
		numElves int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "asdf",
			args: args{
				elves:    "1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000",
				numElves: 1,
			},
			want: []int{24000},
		},
		{
			name: "asdf",
			args: args{
				elves:    "1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000",
				numElves: 3,
			},
			want: []int{24000, 11000, 10000},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findTopCalories(tt.args.elves, tt.args.numElves); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("findTopCalories() = %v, want %v", got, tt.want)
			}
		})
	}
}
