package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func insert(slice []int, index, value int) []int {
	slice = append(slice[:index+1], slice[index:]...)
	slice[index] = value
	return slice

}
func findTopCalories(elves string, numElves int) []int {
	var elfCalories []int = make([]int, numElves)
	var newElfCalories int
	for _, v := range strings.Split(elves, "\n") {
		switch {
		case v == "":
			for i, v := range elfCalories {
				if newElfCalories > v {
					elfCalories = insert(elfCalories, i, newElfCalories)
					elfCalories = elfCalories[:numElves]
					break
				}
			}
			newElfCalories = 0
		default:
			newVal, err := strconv.Atoi(v)
			if err != nil {
				fmt.Errorf("Could not convert %v into an int.\n", v)
			}
			newElfCalories += newVal
		}
	}
	for i, v := range elfCalories {
		if newElfCalories > v {
			elfCalories = insert(elfCalories, i, newElfCalories)
			elfCalories = elfCalories[:numElves]
		}
	}
	return elfCalories
}

func main() {
	content, err := ioutil.ReadFile("input_data.txt")
	if err != nil {
		fmt.Errorf("Could not read input file.\n")
	}
	fmt.Println(findTopCalories(string(content), 1)[0])
	var topSum int
	for _, v := range findTopCalories(string(content), 3) {
		topSum += v
	}
	fmt.Println(topSum)
}
