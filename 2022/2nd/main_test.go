package main

import (
	"io"
	"strings"
	"testing"
)

func Test_convertInput(t *testing.T) {
	type args struct {
		c string
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "A is rock",
			args: args{
				c: "A",
			},
			want:    rock,
			wantErr: false,
		},
		{
			name: "X is rock",
			args: args{
				c: "X",
			},
			want:    rock,
			wantErr: false,
		},
		{
			name: "B is paper",
			args: args{
				c: "B",
			},
			want:    paper,
			wantErr: false,
		},
		{
			name: "Y is paper",
			args: args{
				c: "Y",
			},
			want:    paper,
			wantErr: false,
		},
		{
			name: "C is scissors",
			args: args{
				c: "C",
			},
			want:    scissors,
			wantErr: false,
		},
		{
			name: "Z is scissors",
			args: args{
				c: "Z",
			},
			want:    scissors,
			wantErr: false,
		},
		{
			name: "J is an error",
			args: args{
				c: "J",
			},
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := convertInput(tt.args.c)
			if (err != nil) != tt.wantErr {
				t.Errorf("convertInput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("convertInput() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_calculateScore(t *testing.T) {
	type args struct {
		opponent int
		me       int
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "opponent paper beats rock",
			args: args{
				opponent: paper,
				me:       rock,
			},
			want:    rock + lost,
			wantErr: false,
		},
		{
			name: "opponent paper loses to scissors",
			args: args{
				opponent: paper,
				me:       scissors,
			},
			want:    scissors + won,
			wantErr: false,
		},
		{
			name: "opponent paper draws with paper",
			args: args{
				opponent: paper,
				me:       paper,
			},
			want:    paper + drawn,
			wantErr: false,
		},
		{
			name: "opponent rock beats scissors",
			args: args{
				opponent: rock,
				me:       scissors,
			},
			want:    scissors + lost,
			wantErr: false,
		},
		{
			name: "opponent rock loses to paper",
			args: args{
				opponent: rock,
				me:       paper,
			},
			want:    paper + won,
			wantErr: false,
		},
		{
			name: "opponent rock draws with rock",
			args: args{
				opponent: rock,
				me:       rock,
			},
			want:    rock + drawn,
			wantErr: false,
		},
		{
			name: "opponent scissors beats paper",
			args: args{
				opponent: scissors,
				me:       paper,
			},
			want:    paper + lost,
			wantErr: false,
		},
		{
			name: "opponent scissors loses to rock",
			args: args{
				opponent: scissors,
				me:       rock,
			},
			want:    rock + won,
			wantErr: false,
		},
		{
			name: "opponent scissors draws with scissors",
			args: args{
				opponent: scissors,
				me:       scissors,
			},
			want:    scissors + drawn,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := calculateScore(tt.args.opponent, tt.args.me)
			if (err != nil) != tt.wantErr {
				t.Errorf("calculateScore() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("calculateScore() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_generateResults(t *testing.T) {
	type args struct {
		input io.Reader
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "simple example",
			args: args{
				input: strings.NewReader("A Y"),
			},
			want: paper + won,
		},
		{
			name: "another simple example",
			args: args{
				input: strings.NewReader("B X"),
			},
			want: rock + lost,
		},
		{
			name: "yet another simple example",
			args: args{
				input: strings.NewReader("C Z"),
			},
			want: scissors + drawn,
		},
		{
			name: "combine the simple examples",
			args: args{
				input: strings.NewReader("A Y\nB X\nC Z"),
			},
			want: paper + won + rock + lost + scissors + drawn,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := generateResults(tt.args.input); got != tt.want {
				t.Errorf("generateResults() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_calculatePartTwoScore(t *testing.T) {
	type args struct {
		result   int
		opponent int
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{

		{
			name: "opponent paper beats rock",
			args: args{
				opponent: paper,
				result:   lost,
			},
			want:    rock + lost,
			wantErr: false,
		},
		{
			name: "opponent paper loses to scissors",
			args: args{
				opponent: paper,
				result:   won,
			},
			want:    scissors + won,
			wantErr: false,
		},
		{
			name: "opponent paper draws with paper",
			args: args{
				opponent: paper,
				result:   drawn,
			},
			want:    paper + drawn,
			wantErr: false,
		},
		{
			name: "opponent rock beats scissors",
			args: args{
				opponent: rock,
				result:   lost,
			},
			want:    scissors + lost,
			wantErr: false,
		},
		{
			name: "opponent rock loses to paper",
			args: args{
				opponent: rock,
				result:   won,
			},
			want:    paper + won,
			wantErr: false,
		},
		{
			name: "opponent rock draws with rock",
			args: args{
				opponent: rock,
				result:   drawn,
			},
			want:    rock + drawn,
			wantErr: false,
		},
		{
			name: "opponent scissors beats paper",
			args: args{
				opponent: scissors,
				result:   lost,
			},
			want:    paper + lost,
			wantErr: false,
		},
		{
			name: "opponent scissors loses to rock",
			args: args{
				opponent: scissors,
				result:   won,
			},
			want:    rock + won,
			wantErr: false,
		},
		{
			name: "opponent scissors draws with scissors",
			args: args{
				opponent: scissors,
				result:   drawn,
			},
			want:    scissors + drawn,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := calculatePartTwoScore(tt.args.result, tt.args.opponent)
			if (err != nil) != tt.wantErr {
				t.Errorf("calculatePartTwoScore() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("calculatePartTwoScore() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_generatePartTwoResults(t *testing.T) {
	type args struct {
		input io.Reader
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "simple example",
			args: args{
				input: strings.NewReader("A Y"),
			},
			want: rock + drawn,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := generatePartTwoResults(tt.args.input); got != tt.want {
				t.Errorf("generatePartTwoResults() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_convertPartTwoInput(t *testing.T) {
	type args struct {
		c string
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "A is rock",
			args: args{
				c: "A",
			},
			want:    rock,
			wantErr: false,
		},
		{
			name: "X is lost",
			args: args{
				c: "X",
			},
			want:    lost,
			wantErr: false,
		},
		{
			name: "B is paper",
			args: args{
				c: "B",
			},
			want:    paper,
			wantErr: false,
		},
		{
			name: "Y is drawn",
			args: args{
				c: "Y",
			},
			want:    drawn,
			wantErr: false,
		},
		{
			name: "C is scissors",
			args: args{
				c: "C",
			},
			want:    scissors,
			wantErr: false,
		},
		{
			name: "Z is won",
			args: args{
				c: "Z",
			},
			want:    won,
			wantErr: false,
		},
		{
			name: "J is an error",
			args: args{
				c: "J",
			},
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := convertPartTwoInput(tt.args.c)
			if (err != nil) != tt.wantErr {
				t.Errorf("convertPartTwoInput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("convertPartTwoInput() = %v, want %v", got, tt.want)
			}
		})
	}
}
