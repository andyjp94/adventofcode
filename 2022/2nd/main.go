package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
)

const (
	lost     int = 0
	drawn    int = 3
	won      int = 6
	rock     int = 1
	paper    int = 2
	scissors int = 3
)

// convertInput changes the string inputs to enums
// that are easier to work with
func convertInput(c string) (int, error) {
	switch c {
	case "A", "X":
		return rock, nil
	case "B", "Y":
		return paper, nil
	case "C", "Z":
		return scissors, nil
	default:
		fmt.Errorf("Unexpected input")
	}
	return 0, errors.New("asf")
}

// convertInput changes the string inputs to enums
// that are easier to work with
func convertPartTwoInput(c string) (int, error) {
	switch c {
	case "A":
		return rock, nil
	case "B":
		return paper, nil
	case "C":
		return scissors, nil
	case "X":
		return lost, nil
	case "Y":
		return drawn, nil
	case "Z":
		return won, nil
	default:
		fmt.Errorf("Unexpected input")
	}
	return 0, errors.New("asf")
}

// calculateScore calculates the score based on whether
// the opponent won/drew/lost and what hand was played
func calculateScore(opponent, me int) (int, error) {
	var result int
	switch opponent {
	case scissors:
		switch me {
		case rock:
			result = won
		case paper:
			result = lost
		case scissors:
			result = drawn
		default:
			return 0, errors.New("asdf")
		}
	case paper:
		switch me {
		case rock:
			result = lost
		case paper:
			result = drawn
		case scissors:
			result = won
		default:
			return 0, errors.New("asdf")
		}
	case rock:
		switch me {
		case rock:
			result = drawn
		case paper:
			result = won
		case scissors:
			result = lost
		default:
			return 0, errors.New("asdf")
		}
	default:
		return 0, errors.New("asdf")
	}
	return me + result, nil
}

func calculatePartTwoScore(result, opponent int) (int, error) {
	var score int
	switch result {
	case won:
		switch opponent {
		case rock:
			score = paper
		case paper:
			score = scissors
		case scissors:
			score = rock
		default:
			return 0, errors.New("asdf")
		}
	case drawn:
		score = opponent
	case lost:
		switch opponent {
		case rock:
			score = scissors
		case paper:
			score = rock
		case scissors:
			score = paper
		default:
			return 0, errors.New("asdf")
		}
	}
	return result + score, nil
}
func generateResults(input io.Reader) int {
	scanner := bufio.NewScanner(input)
	var overallScore int
	for scanner.Scan() {
		x := strings.Split(scanner.Text(), " ")
		opponentInput, err := convertInput(x[0])
		if err != nil {
			panic("asdf")
		}
		yourInput, err := convertInput(x[1])
		if err != nil {
			panic("asdf")
		}
		newScore, err := calculateScore(opponentInput, yourInput)
		if err != nil {
			panic("adsfsf")
		}
		overallScore += newScore
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
	return overallScore
}

func generatePartTwoResults(input io.Reader) int {
	scanner := bufio.NewScanner(input)
	var overallScore int
	for scanner.Scan() {
		x := strings.Split(scanner.Text(), " ")
		opponentInput, err := convertPartTwoInput(x[0])
		if err != nil {
			panic("asdf")
		}
		yourInput, err := convertPartTwoInput(x[1])
		if err != nil {
			panic("asdf")
		}
		newScore, err := calculatePartTwoScore(yourInput, opponentInput)
		if err != nil {
			panic("adsfsf")
		}
		overallScore += newScore
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
	return overallScore
}
func main() {
	content, err := os.Open("input_data.txt")
	fmt.Println(generateResults(content))
	content.Seek(0, io.SeekStart)
	fmt.Println(generatePartTwoResults(content))
	if err != nil {
		fmt.Errorf("Could not read input file.\n")
	}
}
