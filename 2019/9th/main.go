package main

import (
	"fmt"
	"sync"

	intcode "gitlab.com/andyjp94/adventofcode/2019/intcode"
)

func main() {

	memArr := intcode.GetMemoryArray("./files/input_data.txt")
	inputChan := make(chan int, 100)
	outputChan := make(chan int, 100)

	var wg sync.WaitGroup
	wg.Add(1)
	go intcode.ProcessEntireProgram(memArr[:], inputChan, outputChan, &wg)
	inputChan <- 1

	wg.Wait()
	for v := range outputChan {

		fmt.Println(v)
	}
	// diagnosticVal := <-outputChan
	// fmt.Println(diagnosticVal)
	return

}
