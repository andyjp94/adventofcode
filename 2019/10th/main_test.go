package main

import "testing"

func TestGetLineXSame(t *testing.T) {
	x := asteroid{pos: coord{x: 0, y: 1}}.getLineEquation(asteroid{pos: coord{x: 0, y: 4}})
	if x(coord{x: 0, y: 3}) != true {
		t.Error("")
	}
	if x(coord{x: 10, y: 3}) == true {
		t.Error("")
	}
}
func TestGetLineYSame(t *testing.T) {
	x := asteroid{pos: coord{x: 0, y: 1}}.getLineEquation(asteroid{pos: coord{x: 2, y: 1}})
	if x(coord{x: 7, y: 1}) != true {
		t.Error("")
	}
	if x(coord{x: 10, y: 3}) == true {
		t.Error("")
	}
}

func TestGetLine(t *testing.T) {
	x := asteroid{pos: coord{x: 0, y: 0}}.getLineEquation(asteroid{pos: coord{x: 1, y: 1}})
	if x(coord{x: 2, y: 2}) != true {
		t.Error("")
	}
	if x(coord{x: 2, y: 3}) == true {
		t.Error("")
	}
}
