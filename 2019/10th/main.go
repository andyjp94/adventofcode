package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type coord struct {
	x int
	y int
}

type asteroid struct {
	pos    coord
	canSee []asteroid
}

type asteroidField struct {
	asteriods []*asteroid
	maxSize   coord
}

func (af *asteroidField) setspacialRange() {
	y, x := 0, 0
	for _, v := range af.asteriods {
		if (*v).pos.x > x {
			x = (*v).pos.x
		}
		if (*v).pos.y > y {
			y = (*v).pos.y
		}
	}
	af.maxSize = coord{x: x, y: y}
}

func processLine(line string) (yPos []int) {
	in, sub := 0, 0

	//Loop until there are no more asteroids
	for in != -1 {
		subLine := line[sub:]
		//Find new asteroid
		in = strings.Index(subLine, "#")
		if in != -1 {
			sub += in + 1
			//Add to the output array
			yPos = append(yPos, sub)
		}
	}
	return
}
func check(e error) {
	if e != nil {
		panic(e)
	}
}

//Create asteroid from multiple y value that all share the same x value
func createAsteroidMultipleY(x int, ys []int) (asteroids []*asteroid) {
	for _, v := range ys {
		newAsteroid := asteroid{pos: coord{x: x, y: v}}
		asteroids = append(asteroids, &newAsteroid)
	}
	return
}

func processFile(path string) (asteroids []*asteroid) {
	file, err := os.Open(path)
	check(err)
	defer file.Close()
	scanner := bufio.NewScanner(file)
	i := 0
	for scanner.Scan() {
		//Get the y positions for this row
		yPoss := processLine(scanner.Text())
		asteroids = append(asteroids, createAsteroidMultipleY(i, yPoss)...)
		i++
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
	return
}

func (a asteroid) getLineEquation(b asteroid) func(c coord) bool {
	if a.pos.x == b.pos.x {
		return func(c coord) bool {
			return a.pos.x == c.x
		}
	} else if a.pos.y == b.pos.y {
		return func(c coord) bool {
			return a.pos.y == c.y
		}
	} else {
		gradient := (a.pos.y - b.pos.y) / (a.pos.x - b.pos.x)
		offset := a.pos.y - gradient*a.pos.x
		return func(c coord) bool {
			return c.y == gradient*c.x+offset
		}
	}
}

func main() {
	var field asteroidField
	field.asteriods = processFile("./files/basic.txt")
	field.setspacialRange()
}
