package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type pixel int
type image struct {
	width  pixel
	height pixel
}

type layer [][]int

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func getlayers(im image, strEncodedValue string) (layers []layer) {
	// strEncodedValue := strconv.Itoa(encodedValue)

	for j := 0; j < len(strEncodedValue); j += int(im.width) * int(im.height) {
		var lay layer
		for i := 0; i < int(im.height); i++ {
			var b []int
			for k := j + i*int(im.width); k < j+i*int(im.width)+int(im.width); k++ {
				char, err := strconv.Atoi(string(strEncodedValue[k]))
				check(err)
				b = append(b, int(char))
			}
			// lay = append(lay, strEncodedValue[j+i*int(im.width):j+i*int(im.width)+int(im.width)])
			lay = append(lay, b)
		}
		layers = append(layers, lay)
	}

	return
}
func getNumValue(l layer, compval int) (total int) {
	for _, row := range l {
		for _, val := range row {
			if val == compval {
				total++
			}
		}
	}
	return
}

func getLine(file string) string {
	f, err := os.Open(file)
	check(err)
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Scan()
	memory := scanner.Text()
	check(err)

	return memory
}

type position struct {
	x int
	y int
}

const (
	black       int = iota
	white       int = iota
	transparent int = iota
)

func getColour(layers []layer, pos position) int {
	var colourArray []int
	for _, l := range layers {
		colourArray = append(colourArray, l[pos.y][pos.x])
	}
	for _, colour := range colourArray {
		if colour != transparent {
			return colour
		}
	}
	return -1
}

func getColours(layers []layer, pos position) (colours [][]int) {
	for y := 0; y < pos.y; y++ {
		var row []int
		for x := 0; x < pos.x; x++ {
			row = append(row, getColour(layers, position{x: x, y: y}))
		}
		colours = append(colours, row)
	}
	return
}

func main() {

	im := image{width: 25, height: 6}
	line := getLine("./files/input_data.txt")
	layers := getlayers(im, line)
	var targetL layer
	var lowestVal int
	for i, l := range layers {
		numZeros := getNumValue(l, 0)
		if i == 0 {
			lowestVal = numZeros
			targetL = l
		} else if lowestVal > numZeros {
			lowestVal = numZeros
			targetL = l
		}
	}

	fmt.Println(getNumValue(targetL, 1) * getNumValue(targetL, 2))

	for _, l := range getColours(layers, position{x: int(im.width), y: int(im.height)}) {
		fmt.Println(l)
	}

}
