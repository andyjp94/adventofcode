package intcode

import (
	"bufio"
	"errors"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
	"sync"

	"go.uber.org/zap"
)

var sugar *zap.SugaredLogger

type opCode int
type pamMode int

const (
	position  pamMode = iota
	immediate pamMode = iota
	relative  pamMode = iota
)

const (
	addition           opCode = iota + 1
	multiplication     opCode = iota + 1
	save               opCode = iota + 1
	output             opCode = iota + 1
	jumpTrue           opCode = iota + 1
	jumpFalse          opCode = iota + 1
	lessThan           opCode = iota + 1
	equals             opCode = iota + 1
	updateRelativeBase opCode = iota + 1
	halt               opCode = 99
)

type param struct {
	mode  pamMode
	value int
}
type comms struct {
	input  <-chan int
	output chan<- int
	wg     *sync.WaitGroup
}

type instruction struct {
	opcode          opCode
	modes           [3]pamMode
	operands        []int
	pointerIncrease int
	index           int
}

func (in *instruction) setModes(modes int) {
	//For each modes:
	// Get the digit in the integer that corresponds to the mode
	for i := 0; i < 3; i++ {
		in.modes[i] = pamMode(int(float64(modes)/math.Pow(10, float64(i))) % 10)

	}

}

func (in instruction) getNumArgs() (num int) {
	switch in.opcode {
	case addition, multiplication, lessThan, equals, jumpTrue, jumpFalse:
		num = 2
	case output, save, updateRelativeBase:
		num = 1
	case halt:
		num = 0
	}
	return
}

//This is a hideous function
func (in *instruction) setOperands(memory []int, relativeBase int) {

	for i := 0; i < in.getNumArgs(); i++ {
		var addressValue int
		switch in.opcode {
		case output, save:
			addressValue = in.index + i + 1
		default:
			addressValue = memory[in.index+i+1]
		}
		switch in.modes[i] {
		case relative:
			in.operands = append(in.operands, relativeBase+memory[in.index+i+1])
		case immediate:
			in.operands = append(in.operands, addressValue)
		case position:
			in.operands = append(in.operands, memory[addressValue])
		}
	}
	switch in.opcode {
	case addition, multiplication, lessThan, equals:
		in.operands = append(in.operands, memory[in.index+3])
	}

}

func (in *instruction) setInstructionPointer() {
	switch in.opcode {
	case addition, multiplication, lessThan, equals:
		in.pointerIncrease = 4
	case jumpTrue, jumpFalse:
		in.pointerIncrease = 3
	case save, output, updateRelativeBase:
		in.pointerIncrease = 2
	}
}

func (in *instruction) setInstruction(memory []int, index int, relativeBase int) {
	in.index = index
	in.opcode = opCode(memory[in.index] % 100)
	sugar.Debugf("Index is %d.", in.index)
	sugar.Debugf("Opcode is %d.", in.opcode)
	in.setModes(memory[index] / 100)
	in.setOperands(memory, relativeBase)
	in.setInstructionPointer()
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func getLine(file string) []string {
	f, err := os.Open(file)
	check(err)
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Scan()
	memory := scanner.Text()
	strArray := strings.Split(memory, ",")

	if err := scanner.Err(); err != nil {
		fmt.Fprintln(f, "reading file:", err)
	}
	return strArray
}
func GetMemoryArray(file string) (memoryArray []int) {
	strArray := getLine(file)
	for _, i := range strArray {
		j, err := strconv.Atoi(i)
		if err != nil {
			panic(err)
		}
		memoryArray = append(memoryArray, j)
	}
	return
}

func (in instruction) runInstruction(memory []int, comm comms, index *int, relativeBase *int) (complete bool, err error) {

	switch in.opcode {
	case addition:
		sugar.Infof("Operation: Add %d and %d to memory address %d", in.operands[0], in.operands[1], in.operands[2])
		memory[in.operands[2]] = in.operands[1] + in.operands[0]

	case multiplication:
		sugar.Infof("Operation: Multiply %d and %d to memory address %d", in.operands[0], in.operands[1], in.operands[2])
		memory[in.operands[2]] = in.operands[1] * in.operands[0]
	case lessThan:
		sugar.Infof("Operation: Check if %d is less than %d", in.operands[0], in.operands[1])
		if in.operands[1] > in.operands[0] {
			memory[in.operands[2]] = 1
		} else {
			memory[in.operands[2]] = 0
		}

	case equals:
		sugar.Infof("Operation: Check if %d is equal to %d", in.operands[0], in.operands[1])
		if in.operands[1] == in.operands[0] {
			memory[in.operands[2]] = 1
		} else {
			memory[in.operands[2]] = 0
		}
	case jumpTrue:
		sugar.Infof("Operation: Jump to %d if %d is not zero", in.operands[1], in.operands[0])
		if in.operands[0] != 0 {
			sugar.Info("Jumped")
			*index = in.operands[1]
			return
		}
		sugar.Info("Did not Jump")

	case jumpFalse:
		sugar.Infof("Operation: Jump to %d if %d is  zero", in.operands[1], in.operands[0])
		if in.operands[0] == 0 {
			sugar.Info("Jumped")
			*index = in.operands[1]
			return
		}
		sugar.Info("Did not Jump")

	case save:
		sugar.Info("Waiting for input")
		memory[in.operands[0]] = <-comm.input
		sugar.Infof("Operation: Saved %d to memory address %d", memory[in.operands[0]], in.operands[0])

	case output:

		comm.output <- memory[in.operands[0]]
		sugar.Infof("Operation: Output %d", memory[in.operands[0]])
	case updateRelativeBase:
		sugar.Infof("Operation: Update relative base from %d to %d", *relativeBase, *relativeBase+in.operands[0])
		*relativeBase += in.operands[0]
	case halt:
		sugar.Info("Operation: Program halted.\n")
		comm.wg.Done()
		close(comm.output)
		complete = true

	default:
		sugar.Errorf("Opcode did not match expected value: %d\n", in.opcode)
		err = errors.New("")
	}
	*index += in.pointerIncrease
	return
}
func processOpcode(memory []int, comm comms, index *int, relativeBase *int) (complete bool, err error) {
	var in instruction

	in.setInstruction(memory, *index, *relativeBase)

	complete, err = in.runInstruction(memory[:], comm, index, relativeBase)
	sugar.Debug("Processed Opcode")

	return
}

func ProcessEntireProgram(memArr []int, input <-chan int, output chan<- int, wg *sync.WaitGroup) {
	setLogging()

	instructionPointer := new(int)
	relativeBase := new(int)
	tmpArr := make([]int, 500*len(memArr))
	memArr = append(memArr, tmpArr...)
	comm := comms{input: input, output: output, wg: wg}
	var err error
	var complete bool
	for *instructionPointer != -1 {
		complete, err = processOpcode(memArr[:], comm, instructionPointer, relativeBase)
		check(err)
		if complete {
			break
		}
	}
	sugar.Sync()

}

func setLogging() {
	logger, _ := zap.NewDevelopment()

	sugar = logger.Sugar()
}
