package main

func main() {
	setLogging()

	memArr := GetMemoryArray("./files/input_data.txt")
	inputs := []int{5}
	ProcessEntireProgram(memArr[:], inputs)
	sugar.Sync()
}
