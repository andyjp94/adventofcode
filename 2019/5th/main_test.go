package intcode

import (
	"fmt"
	"sync"
	"testing"
)

func TestCreateBasicInstruction(t *testing.T) {
	setLogging()
	for p, v := range [4]opCode{addition, multiplication, save, output} {
		var in instruction
		in.setInstruction([]int{p + 1, 0, 0, 0, 0}, 0, "A")
		switch {
		case in.opcode != v:
			t.Error()
		case in.modes != [3]pamMode{position, position, position}:
			t.Error()
		}
	}
}

func TestCreateBasicPositionalModes(t *testing.T) {
	setLogging()

	inputs := map[int][3]pamMode{
		1:     [3]pamMode{position, position, position},
		101:   [3]pamMode{immediate, position, position},
		1001:  [3]pamMode{position, immediate, position},
		1101:  [3]pamMode{immediate, immediate, position},
		10001: [3]pamMode{position, position, immediate},
		10101: [3]pamMode{immediate, position, immediate},
		11001: [3]pamMode{position, immediate, immediate},
		11101: [3]pamMode{immediate, immediate, immediate},
	}
	for key, value := range inputs {
		var in instruction
		in.setInstruction([]int{key, 0, 0, 0, 0, 0}, 0, "A")
		switch {
		case in.opcode != addition:
			t.Error()
		case in.modes != value:
			t.Error()
			fmt.Println(key, in.modes)
		}
	}
}

// func TestRunInstructionAddPosition(t *testing.T) {
// 	setLogging()
// 	in := instruction{
// 		opcode:          addition,
// 		modes:           [3]pamMode{position, position, position},
// 		operands:        []int{4, 1, 1},
// 		index:           0,
// 		pointerIncrease: 4,
// 	}
// 	memory := []int{1, 1, 1, 1, 5}
// 	index := new(int)
// 	*index = 1
// 	in.runInstruction(memory[:], 1, index)
// 	for i, v := range []int{1, 6, 1, 1} {
// 		if memory[i] != v {
// 			t.Error()
// 		}
// 	}
// }
// func TestRunInstructionAddImmediate(t *testing.T) {
// 	setLogging()
// 	in := instruction{
// 		opcode:          addition,
// 		modes:           [3]pamMode{immediate, immediate, immediate},
// 		operands:        []int{1, 1, 1},
// 		index:           0,
// 		pointerIncrease: 4,
// 	}
// 	memory := []int{1, 1, 1, 1}
// 	index := new(int)
// 	*index = 1
// 	inputChan := make(chan int)
// 	outputChan := make(chan int)
// 	in.runInstruction(memory[:], inputChan, outputChan, index, "a")
// 	// fmt.Println(<-outputChan)
// 	for i, v := range []int{1, 2, 1, 1} {
// 		if memory[i] != v {
// 			t.Error()
// 		}
// 	}
// }
// func TestRunInstructionMultiplyImmediate(t *testing.T) {
// 	setLogging()
// 	in := instruction{
// 		opcode:          multiplication,
// 		modes:           [3]pamMode{immediate, immediate, immediate},
// 		operands:        []int{2, 2, 1},
// 		index:           0,
// 		pointerIncrease: 4,
// 	}
// 	memory := []int{2, 2, 2, 1}
// 	index := new(int)
// 	*index = 1
// 	inputChan := make(chan int)
// 	outputChan := make(chan int)
// 	in.runInstruction(memory[:], inputChan, outputChan, index, "a")
// 	for i, v := range []int{2, 4, 2, 1} {
// 		if memory[i] != v {
// 			t.Errorf("\n%d\r\n%d\n", memory[i], v)
// 		}
// 	}
// }

func as(file string, input int) (diagnosticVal int) {
	memArr := GetMemoryArray(file)
	inputChan := make(chan int)
	outputChan := make(chan int)

	var wg sync.WaitGroup
	wg.Add(1)
	go ProcessEntireProgram(memArr[:], inputChan, outputChan, &wg, 0)
	inputChan <- input

	diagnosticVal = <-outputChan
	
	wg.Wait()
	return diagnosticVal
}

func TestEqualPositionTrue(t *testing.T) {
	setLogging()

	if as("./files/equal_pos_test.txt", 8) != 1 {
		t.Errorf("Positional Equality Success test failed.")
	}

}

func TestEqualPositionFalse(t *testing.T) {
	setLogging()
	if as("./files/equal_pos_test.txt", 7) != 0 {
		t.Errorf("Positional Equality Success test failed.")
	}

}

func TestEqualImmediateTrue(t *testing.T) {
	setLogging()
	if as("./files/equal_im_test.txt", 8) == 0 {
		t.Errorf("Immediate Equality Success test failed.")
	}

}

func TestEqualImmediateFalse(t *testing.T) {
	setLogging()
	if as("./files/equal_im_test.txt", 7) != 0 {
		t.Errorf("Immediate Equality Failure test failed.")
	}

}

func TestLessThanPositionTrue(t *testing.T) {
	setLogging()
	if as("./files/less_pos_test.txt", 7) == 0 {
		t.Errorf("Positional Less Than True test failed.")
	}

}
func TestLessThanPositionFalse(t *testing.T) {
	setLogging()
	if as("./files/less_pos_test.txt", 9) != 0 {
		t.Errorf("Positional Less than False test failed.")
	}

}

func TestJumpPositionalOne(t *testing.T) {
	setLogging()
	if as("./files/jump_pos_test.txt", 1) != 1 {
		t.Errorf("Positional Jump One test failed.")
	}
}

func TestJumpPositionalZero(t *testing.T) {
	setLogging()
	if as("./files/jump_pos_test.txt", 0) != 0 {
		t.Errorf("Positional Jump Zero test failed.")
	}

}

func TestJumpImmediateOne(t *testing.T) {
	setLogging()
	if as("./files/jump_im_test.txt", 1) != 1 {
		t.Errorf("Immediate Jump One test failed.")
	}

}

func TestJumpImmediateZero(t *testing.T) {
	setLogging()
	if as("./files/jump_im_test.txt", 0) != 0 {
		t.Errorf("Immediate Jump Zero test failed.")
	}
}

func TestGreaterThanEight(t *testing.T) {
	setLogging()
	if as("./files/complex_test.txt", 9) != 1001 {
		t.Errorf("Greater than eight failed.")
	}
}

func TestLessThanEight(t *testing.T) {
	setLogging()
	if as("./files/complex_test.txt", 7) != 999 {
		t.Errorf("Less than eight failed.")
	}
}

func TestEqualToEight(t *testing.T) {
	setLogging()
	if as("./files/complex_test.txt", 8) != 1000 {
		t.Errorf("Equal to eight failed.")
	}
}
