package main

import (
	"fmt"
	"sync"

	intcode "gitlab.com/andyjp94/adventofcode/2019/intcode"
	"go.uber.org/zap"
)

const (
	a int = iota
	b int = iota
	c int = iota
	d int = iota
	e int = iota
)

func tryCombination(comb []int, memory []int) int {
	cs := []chan int{
		make(chan int, 100),
		make(chan int, 100),
		make(chan int, 100),
		make(chan int, 100),
		make(chan int, 100),
	}
	var wg sync.WaitGroup

	for i := a; i < e+1; i++ {
		wg.Add(1)
		tMemory := make([]int, len(memory))
		copy(tMemory, memory)
		if i == e {
			go intcode.ProcessEntireProgram(tMemory, cs[i], cs[a], &wg)
		} else {
			go intcode.ProcessEntireProgram(tMemory, cs[i], cs[i+1], &wg)
		}
	}

	for i, phase := range comb {
		cs[i] <- phase
	}

	cs[a] <- 0

	wg.Wait()

	return <-cs[a]
}

var sugar *zap.SugaredLogger

func setLogging() {
	logger, _ := zap.NewDevelopment()

	sugar = logger.Sugar()
}

func tryPhases(phases []int, memArr []int) (result int) {

	for _, e := range phases {
		for _, d := range phases {
			for _, c := range phases {
				for _, b := range phases {
					for _, a := range phases {
						if !(a == b || a == c || a == d || a == e || b == c || b == d || b == e || c == d || c == e || d == e) {
							tmpResult := tryCombination([]int{a, b, c, d, e}, memArr)
							if tmpResult > result {
								result = tmpResult
							}
						}

					}
				}
			}
		}
	}
	return
}
func main() {
	phases := []int{5, 6, 7, 8, 9}

	memArr := intcode.GetMemoryArray("./files/input_data.txt")
	fmt.Printf("The answer is: %d\n", tryPhases(phases, memArr))

}
