package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type planet struct {
	name      string
	orbits    *planet
	orbitedBy []*planet
	depth     int
}

func readFile(file string) (conns []string) {
	f, err := os.Open(file)
	check(err)
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		conns = append(conns, line)
	}

	if err := scanner.Err(); err != nil {
		fmt.Fprintln(f, "reading file:", err)
	}
	return
}

func findCentre(conns []string) (planet, error) {
	for _, conn := range conns {
		planets := strings.Split(conn, ")")
		if "COM" == planets[0] {
			com := planet{
				name:      planets[0],
				orbits:    nil,
				orbitedBy: nil,
				depth:     0,
			}
			newPlanet := planet{
				name:      planets[1],
				orbitedBy: nil,
				orbits:    nil,
				depth:     1,
			}
			com.orbitedBy = append(com.orbitedBy, &newPlanet)
			newPlanet.orbits = &com
			return newPlanet, nil
		}
	}
	return planet{"", nil, nil, 0}, errors.New("COM was not found.")
}

// func (p planet) findNode(finish planet) {
// 	//go to parent
// 	//scan children of parent
// 	//if there then get difference in depth
// 	// if not then repeat
// 	parent := p.orbits
// 	if parent.name == "SAN" {
// 		return
// 	}
// 	for i := 0; i < len(p.orbitedBy); i++ {
// 		parent.orbitedBy[i].findNode(finish)
// 	}

// 	if
// 	return

// }
func (p *planet) findPlanet(conns []string) {

	for _, conn := range conns {
		planets := strings.Split(conn, ")")
		if p.name == planets[0] {
			p.orbitedBy = append(p.orbitedBy, &planet{
				name:      planets[1],
				orbits:    p,
				orbitedBy: nil,
				depth:     p.depth + 1,
			})
		}
	}

}

func (p planet) checkParent(name string) int {
	var tmp planet
	tmp = p.orbits.findNode(name)
	if tmp.depth != -1 {
		return tmp.depth - p.depth
	}

	a := p.orbits.checkParent(name)
	if a != -1 {
		return a + 1
	}

	return -1

}

func (p planet) findNode(name string) planet {
	//If the planet has the same name return it
	if p.name == name {
		return p
	}
	var tmp planet
	for i := 0; i < len(p.orbitedBy); i++ {
		//if the child has the same name return it
		tmp = p.orbitedBy[i].findNode(name)
		if tmp.depth != -1 {
			return tmp
		}
	}
	return planet{"", nil, nil, -1}
}

func (p *planet) setChildren(conns []string) {

	p.findPlanet(conns)
	for i := 0; i < len(p.orbitedBy); i++ {
		p.orbitedBy[i].setChildren(conns)
	}
}

func (p planet) sumOrbits() int {

	var depth int
	for i := 0; i < len(p.orbitedBy); i++ {
		depth += p.orbitedBy[i].sumOrbits()
	}

	return p.depth + depth
}

func main() {
	conns := readFile("./files/input_data.txt")
	// conns := readFile("./files/small_test.txt")
	planet, err := findCentre(conns)
	check(err)
	planet.setChildren(conns)
	fmt.Println(planet.sumOrbits())
	you := planet.findNode("YOU")
	you.findNode("SAN")

	// fmt.Println(planet.findNode("SAN"))
	fmt.Println(you.checkParent("SAN"))
	// planet.checkParent("SAN")
}
