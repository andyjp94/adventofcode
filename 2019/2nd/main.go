package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func getLine(file string) []string {
	f, err := os.Open(file)
	check(err)
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Scan()
	memory := scanner.Text()
	strArray := strings.Split(memory, ",")

	if err := scanner.Err(); err != nil {
		fmt.Fprintln(f, "reading file:", err)
	}
	return strArray
}
func getMemoryArray(file string) (memoryArray []int) {
	strArray := getLine(file)
	for _, i := range strArray {
		j, err := strconv.Atoi(i)
		if err != nil {
			panic(err)
		}
		memoryArray = append(memoryArray, j)
	}
	return
}

func processOpcode(memory []int, index int) bool {
	if memory[index] == 99 {
		return false
	}

	operandOne := memory[index+1]
	operandTwo := memory[index+2]
	operandOutput := memory[index+3]
	switch memory[index] {
	case 1:
		memory[operandOutput] = memory[operandOne] + memory[operandTwo]
	case 2:
		memory[operandOutput] = memory[operandOne] * memory[operandTwo]
	}
	return true
}

func processEntireProgram(memArr []int) {
	for i := 0; i <= len(memArr); i += 4 {
		exit := processOpcode(memArr[:], i)
		if exit == false {
			break
		}
	}
}

func main() {
out:
	for noun := 0; noun <= 99; noun++ {
		for verb := 0; verb <= 99; verb++ {
			memArr := getMemoryArray("./files/input_data.txt")
			memArr[1] = noun
			memArr[2] = verb
			processEntireProgram(memArr[:])
			if memArr[0] == 19690720 {
				fmt.Printf("answer: %d, noun: %d, verb: %d\n", noun*100+verb, noun, verb)
				break out
			}
		}
	}
}
