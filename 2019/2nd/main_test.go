package main

import "testing"

func TestAddition(t *testing.T) {
	mem := [5]int{1, 0, 0, 0, 99}
	processOpcode(mem[:], 0)
	if [5]int{2, 0, 0, 0, 99} != mem {
		t.Error()
	}
}
func TestMultiplication(t *testing.T) {
	mem := [5]int{2, 3, 0, 3, 99}
	processOpcode(mem[:], 0)
	if [5]int{2, 3, 0, 6, 99} != mem {
		t.Error()
	}
}

func TestComplexMultiplication(t *testing.T) {
	mem := [6]int{2, 4, 4, 5, 99, 0}
	processOpcode(mem[:], 0)
	if [6]int{2, 4, 4, 5, 99, 9801} != mem {
		t.Error()
	}
}
func TestEntireProgram(t *testing.T) {
	mem := [9]int{1, 1, 1, 4, 99, 5, 6, 0, 99}
	processEntireProgram(mem[:])
	if [9]int{30, 1, 1, 4, 2, 5, 6, 0, 99} != mem {
		t.Error()
	}
}
