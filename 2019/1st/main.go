package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func getFuel(mass int) int {

	fuel := int(math.Floor(float64(mass/3))) - 2
	if fuel <= 0 {
		return 0
	}
	return fuel + getFuel(fuel)
}

func getTotalFuel(f *os.File) int {
	var result int

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		text := scanner.Text()
		mass, err := strconv.Atoi(text)
		check(err)

		result += getFuel(mass)

	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(f, "reading file:", err)
	}
	return result
}

func main() {
	f, err := os.Open("./files/input_data.txt")
	check(err)
	defer f.Close()
	fmt.Println(getTotalFuel(f))
}
