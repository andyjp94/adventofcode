package main

import "fmt"

type password []int

func passwordLengthValid(attempt int) bool {
	return 1000000 > attempt
}

func (p password) passwordAdjacentMatches() bool {
	lastV := p[0]
	for _, v := range p[1:] {
		if lastV == v {
			return true
		}
		lastV = v
	}
	return false
}

func (p password) passwordSmallGroup() bool {
	lastV := p[0]
	consecutive := 0
	for _, v := range p[1:] {
		//if the current character matches the last one
		if lastV == v {
			consecutive++
		} else {
			//if the character has changed but the previous two matched
			if consecutive == 1 {
				return true
			}
			consecutive = 0
		}
		lastV = v
	}
	//if the last character matched the previous character
	if consecutive == 1 {
		return true
	}
	return false
}

func (p password) passwordNoDecrease() bool {
	lastV := p[0]
	for _, v := range p[1:] {
		if v > lastV {
			return false
		}
		lastV = v
	}
	return true
}

func createPassword(attempt int) (pass password) {
	var digit int
	for attempt > 0 {
		digit = attempt % 10
		attempt /= 10
		pass = append(pass, digit)
	}
	return
}
func getValidPassword(attempt int) bool {
	pass := createPassword(attempt)
	if passwordLengthValid(attempt) && pass.passwordSmallGroup() && pass.passwordNoDecrease() {
		// fmt.Printf("attempt: %d, %t\n", attempt, pass.passwordSmallGroup())
		return true
	}
	return false

}

func getNumValidPasswords(min, max int) (numValidPasswords int) {

	for i := min; i <= max; i++ {
		if getValidPassword(i) {
			numValidPasswords++
		}
	}
	return
}

func main() {
	fmt.Printf("Part one answer: %d\n", getNumValidPasswords(264360, 746325))
}
