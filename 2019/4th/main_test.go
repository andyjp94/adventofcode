package main

import "testing"

func TestCriteriaMet(t *testing.T) {
	if getValidPassword(111111) {
		t.Error()
	}
}

func TestDecreasing(t *testing.T) {
	if getValidPassword(223450) {
		t.Error()
	}
}

func TestNoDouble(t *testing.T) {
	if getValidPassword(123789) {
		t.Error()
	}
}

func TestLargeGroup(t *testing.T) {
	if getValidPassword(123444) {
		t.Error()
	}
}

func TestCombinedGroups(t *testing.T) {
	if !getValidPassword(111122) {
		t.Error()
	}
}

func TestAdditionalGroup(t *testing.T) {
	if !getValidPassword(668889) {
		t.Error()
	}
}
