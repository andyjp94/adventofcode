package main

import (
	"fmt"
	"testing"
)

func TestCreateRoute(t *testing.T) {
	input := []string{"U2", "R3", "D1", "L2"}
	output := createRoute(input[:])
	expected := []coord{
		coord{x: 0, y: 1},
		coord{x: 0, y: 2},
		coord{x: 1, y: 2},
		coord{x: 2, y: 2},
		coord{x: 3, y: 2},
		coord{x: 3, y: 1},
		coord{x: 2, y: 1},
		coord{x: 1, y: 1},
	}

	if len(expected) != len(output) {
		t.Error()
	}
	for i, Coord := range expected {
		if Coord != output[i] {
			t.Error()
		}
	}
}

func TestGetVector(t *testing.T) {
	v := getVector("R432")
	if v.magnitude != 432 && v.direction == byte('R') {
		t.Error()
	}
}

func TestGetIntersectionsSimple(t *testing.T) {
	intersects := getIntersections([]coord{
		coord{x: 0, y: 1},
		coord{x: 0, y: 2},
		coord{x: 0, y: 3},
		coord{x: 0, y: 4},
	}, []coord{
		coord{x: 0, y: 1},
		coord{x: 1, y: 2},
		coord{x: 1, y: 3},
		coord{x: 1, y: 4},
	})
	expectedResult := coord{x: 0, y: 1}
	if len(intersects) != 1 {
		t.Error()
	}
	if intersects[0] != expectedResult {
		t.Error()
	}
}

func TestEndToEndClosest(t *testing.T) {
	routeA := createRoute([]string{"R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"})
	routeB := createRoute([]string{"U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"})
	allIntersects := getIntersections(routeA, routeB)
	closestCoord := getLowestManhattan(allIntersects)

	if closestCoord.getManhattanDistance(coord{0, 0}) != 159 {
		t.Error()
	}

}

func TestEndToEndStep(t *testing.T) {
	routeA := createRoute([]string{"R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"})
	routeB := createRoute([]string{"U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"})
	allIntersects := getIntersections(routeA, routeB)
	lowestStepCoord := getLowestStep(allIntersects, []coordArr{routeA, routeB})
	fmt.Println(lowestStepCoord.getSteps([]coordArr{routeA, routeB}))
	if lowestStepCoord.getSteps([]coordArr{routeA, routeB}) != 610 {
		t.Error()
	}

}
