package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

type coord struct {
	x int
	y int
}

type coordArr []coord

type vector struct {
	magnitude int
	direction byte
}

func (c coord) getManhattanDistance(comp coord) int {
	return int(math.Abs(float64(comp.x-c.x)) + math.Abs(float64(comp.y-c.y)))
}

func (c coordArr) getPosition(pos coord) int {
	for p, v := range c {
		if v == pos {
			return p
		}
	}
	return -1
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func getVector(pos string) vector {
	magnitude, err := strconv.Atoi(pos[1:])
	check(err)
	return vector{
		magnitude: magnitude,
		direction: byte(pos[0]),
	}
}

func createRoute(pos []string) (route []coord) {
	for _, strVector := range pos {
		var lastPos coord

		vec := getVector(strVector)
		for i := 0; i < vec.magnitude; i++ {
			if len(route) > 0 {
				lastPos = route[len(route)-1]
			} else {
				lastPos = coord{0, 0}
			}

			switch vec.direction {
			case 'R':
				route = append(route, coord{x: lastPos.x + 1, y: lastPos.y})
			case 'L':
				route = append(route, coord{x: lastPos.x - 1, y: lastPos.y})
			case 'U':
				route = append(route, coord{y: lastPos.y + 1, x: lastPos.x})
			case 'D':
				route = append(route, coord{y: lastPos.y - 1, x: lastPos.x})
			}
		}
	}
	return
}

func createRoutes(fileName string) (routeA, routeB []coord) {
	file, err := os.Open(fileName)
	check(err)
	defer file.Close()
	scanner := bufio.NewScanner(file)

	scanner.Scan()
	routeA = createRoute(strings.Split(scanner.Text(), ","))
	scanner.Scan()
	routeB = createRoute(strings.Split(scanner.Text(), ","))
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(file, "reading file:", err)
	}
	return
}

func getIntersections(routeA []coord, routeB []coord) (intersects []coord) {
	for _, posA := range routeA {
		for _, posB := range routeB {
			if posA == posB {
				intersects = append(intersects, posA)
			}
		}
	}
	return
}
func getLowestManhattan(coords []coord) (lowest coord) {
	lowest = coords[0]
	for _, tempCoord := range coords {
		if lowest.getManhattanDistance(coord{0, 0}) > tempCoord.getManhattanDistance(coord{0, 0}) {
			lowest = tempCoord
		}
	}
	return
}

func (c coord) getSteps(routes []coordArr) (steps int) {
	for _, route := range routes {
		steps += route.getPosition(c) + 1
	}
	return
}

func getLowestStep(coords coordArr, routes []coordArr) (lowest coord) {
	lowest = coords[0]
	for _, pos := range coords {
		if lowest.getSteps(routes) > pos.getSteps(routes) {
			lowest = pos
		}
	}
	return
}

func main() {
	routeA, routeB := createRoutes("./files/input_data.txt")
	allIntersects := getIntersections(routeA, routeB)
	closestCoord := getLowestManhattan(allIntersects)
	lowestStepCoord := getLowestStep(allIntersects, []coordArr{routeA, routeB})
	fmt.Printf("Part 1 Answer: %d, x value: %d, y value: %d\n", closestCoord.getManhattanDistance(coord{0, 0}), closestCoord.x, closestCoord.y)
	fmt.Printf("Part 2 Answer: %d, x value: %d, y value: %d\n", lowestStepCoord.getSteps([]coordArr{routeA, routeB}), lowestStepCoord.x, lowestStepCoord.y)
}
