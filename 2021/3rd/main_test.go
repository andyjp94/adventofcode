package main

import (
	"testing"
)

func Test_calculateGamma16(t *testing.T) {
	type args struct {
		diagnostics []uint16
		width       int
	}
	tests := []struct {
		name  string
		args  args
		gamma uint16
		// epsilon uint16
	}{
		{
			name: "Test Provided",
			args: args{
				diagnostics: []uint16{0b00100, 0b11110, 0b10110, 0b10111, 0b10101, 0b01111, 0b00111, 0b11100, 0b10000, 0b11001, 0b00010, 0b01010},
				width:       5,
			},
			gamma: 0b10110,
			// epsilon: 0b01001,
		},
		{
			name: "Another 5 word input",
			args: args{
				diagnostics: []uint16{0b00100, 0b01110, 0b00110, 0b00111, 0b00101, 0b01111, 0b00111, 0b01100, 0b00000, 0b01001, 0b00010, 0b01010},
				width:       5,
			},
			gamma: 0b00110,
			// epsilon: 0b11001,
		},
		{
			name: "12 word input",
			args: args{
				diagnostics: []uint16{
					0b011101101110, 0b010110001101, 0b100111000110, 0b011110101000, 0b101101000100},
				width: 12,
			},
			gamma: 0b011111001100,
			// epsilon: 0b100000110011
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gamma := calculateGamma16(tt.args.diagnostics, tt.args.width); gamma != tt.gamma {
				t.Errorf("calculateGamma() = %v, want %v", gamma, tt.gamma)
			}
		})
	}
}

func Test_calculateEpsilon16(t *testing.T) {
	type args struct {
		gamma uint16
		width int
	}
	tests := []struct {
		name    string
		args    args
		epsilon uint16
	}{
		{
			name: "Test Provided",
			args: args{
				gamma: 0b10110,
				width: 5,
			},

			epsilon: 0b01001,
		},
		{
			name: "Another 5 word input",
			args: args{
				gamma: 0b00110,
				width: 5,
			},
			epsilon: 0b11001,
		},
		{
			name: "12 word input",
			args: args{
				gamma: 0b011111001100,
				width: 12,
			},
			epsilon: 0b100000110011,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if epsilon := calculateEpsilon16(tt.args.gamma, tt.args.width); epsilon != tt.epsilon {
				t.Errorf("calculateEpsilon() = %v, want %v", epsilon, tt.epsilon)
			}
		})
	}
}
func Test_calculatePower(t *testing.T) {
	type args struct {
		diagnostics []uint16
		width       int
	}
	tests := []struct {
		name  string
		args  args
		power uint32
	}{
		{
			name: "Test Provided",
			args: args{
				diagnostics: []uint16{0b00100, 0b11110, 0b10110, 0b10111, 0b10101, 0b01111, 0b00111, 0b11100, 0b10000, 0b11001, 0b00010, 0b01010},
				width:       5,
			},
			power: 198,
		},
		{
			name: "12 word input",
			args: args{
				diagnostics: []uint16{
					0b011101101110, 0b010110001101, 0b100111000110, 0b011110101000, 0b101101000100},
				width: 12,
			},
			power: uint32(4189604),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculatePower16(tt.args.diagnostics, tt.args.width); got != tt.power {
				t.Errorf("calculatePower() = %v, want %v", got, tt.power)
			}
		})
	}
}

func Test_calculateOxygen(t *testing.T) {
	type args struct {
		diagnostics []uint16
		width       int
	}
	tests := []struct {
		name string
		args args
		want uint16
	}{
		{
			name: "Test Provided",
			args: args{
				diagnostics: []uint16{0b00100, 0b11110, 0b10110, 0b10111, 0b10101, 0b01111, 0b00111, 0b11100, 0b10000, 0b11001, 0b00010, 0b01010},
				width:       5,
			},
			want: 0b10111,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculateGas(Oxygen, tt.args.diagnostics, tt.args.width); got != tt.want {
				t.Errorf("calculateOxygen() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_calculateCO2(t *testing.T) {
	type args struct {
		diagnostics []uint16
		width       int
	}
	tests := []struct {
		name string
		args args
		want uint16
	}{
		{
			name: "Test Provided",
			args: args{
				diagnostics: []uint16{0b00100, 0b11110, 0b10110, 0b10111, 0b10101, 0b01111, 0b00111, 0b11100, 0b10000, 0b11001, 0b00010, 0b01010},
				width:       5,
			},
			want: 0b01010,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculateGas(CarbonDioxide, tt.args.diagnostics, tt.args.width); got != tt.want {
				t.Errorf("calculateCO2() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_calculateLifeSupport(t *testing.T) {
	type args struct {
		diagnostics []uint16
		width       int
	}
	tests := []struct {
		name string
		args args
		want uint32
	}{
		{
			name: "Test Provided",
			args: args{
				diagnostics: []uint16{0b00100, 0b11110, 0b10110, 0b10111, 0b10101, 0b01111, 0b00111, 0b11100, 0b10000, 0b11001, 0b00010, 0b01010},
				width:       5,
			},
			want: 230,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculateLifeSupport(tt.args.diagnostics, tt.args.width); got != tt.want {
				t.Errorf("calculateLifeSupport() = %v, want %v", got, tt.want)
			}
		})
	}
}
