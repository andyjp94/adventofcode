package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

type Gas uint8

const (
	Oxygen Gas = iota
	CarbonDioxide
)

func calculatePower16(diagnostics []uint16, width int) uint32 {
	gamma := calculateGamma16(diagnostics, width)
	epsilon := calculateEpsilon16(gamma, width)

	return uint32(gamma) * uint32(epsilon)
}

func calculateLifeSupport(diagnostics []uint16, width int) uint32 {
	return uint32(calculateGas(Oxygen, diagnostics, width)) * uint32(calculateGas(CarbonDioxide, diagnostics, width))
}

func calculateGas(gas Gas, diagnostics []uint16, width int) uint16 {
	values := diagnostics
	for shift := width - 1; shift >= 0; shift-- {
		var oneCounter float32
		var top_set, bottom_set []uint16
		var half float32 = float32(len(values)) / 2
		for _, v := range values {
			var currentVal = v >> shift & 1
			if currentVal == 1 {
				oneCounter++
				top_set = append(top_set, v)
			} else {
				bottom_set = append(bottom_set, v)
			}
		}
		switch gas {
		case Oxygen:
			if oneCounter >= half {
				values = top_set
			} else {
				values = bottom_set
			}
		case CarbonDioxide:
			if oneCounter >= half {
				values = bottom_set
			} else {
				values = top_set
			}
		}

		if len(values) == 1 {
			return values[0]
		}
		oneCounter = 0
	}
	return values[0]
}

func calculateGamma16(diagnostics []uint16, width int) uint16 {
	var gamma uint16 = 0
	var over_half float32 = float32(len(diagnostics) / 2)
	for shift := width - 1; shift >= 0; shift-- {
		var gammaCounter float32
		for _, v := range diagnostics {
			var currentVal = v >> shift & 1
			if currentVal == 1 {
				gammaCounter++
			}
			if gammaCounter > over_half {
				gamma |= 1 << shift
				gammaCounter = 0
				break
			}
		}
	}

	return gamma
}
func calculateEpsilon16(gamma uint16, width int) uint16 {
	var reset_value uint16 = ^(uint16(0xFFFF) >> (16 - width))
	return ^gamma &^ reset_value
}

func main() {

	content, err := ioutil.ReadFile("input_data.txt")

	if err != nil {
		log.Fatal(err)
	}
	raw_diagnostics := strings.Split(string(content), "\n")
	var binary_diagnostics []uint16
	for _, v := range raw_diagnostics {
		if s, err := strconv.ParseUint(v, 2, 16); err == nil {
			binary_diagnostics = append(binary_diagnostics, uint16(s))
		} else {
			panic(err)
		}
	}
	fmt.Println(len(raw_diagnostics[0]))
	power := calculatePower16(binary_diagnostics, len(raw_diagnostics[0]))
	life_support := calculateLifeSupport(binary_diagnostics, len(raw_diagnostics[0]))
	fmt.Printf("Power Consumption: %d\n", power)
	fmt.Printf("Power Consumption: %d\n", life_support)

}
