package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type population [][]uint8

func (p *population) create(lines []string) {
	*p = make([][]uint8, len(lines))
	for i := range *p {
		(*p)[i] = make([]uint8, len(lines[0]))
	}
	for i, line := range lines {
		for j, char := range line {
			newOctopus, err := strconv.Atoi(string(char))
			(*p)[i][j] = uint8(newOctopus)
			if err != nil {
				panic(err)
			}
		}
	}
}

func (p *population) increment() {
	for i := range *p {
		for j := range (*p)[i] {
			(*p)[i][j]++
		}
	}
}
func (p *population) resetFlash() {
	for row := range *p {
		for column, v := range (*p)[row] {
			if v > 9 {
				(*p)[row][column] = 0
			}
		}
	}
}

func (p population) String() string {
	var s string
	for row := range p {
		s += fmt.Sprintf("%v\n", p[row])
	}
	return s
}

func (p population) size() int {
	return len(p) * len(p[0])
}

type coord struct {
	row, column int
}

func (p population) flash(newP population, alreadyFlashed [][]bool) (population, int) {
	if len(newP) == 0 {
		newP = p
	}
	var oldFlashed [][]bool
	if len(alreadyFlashed) == 0 {

		alreadyFlashed = make([][]bool, len(p))
		for i := range p {
			alreadyFlashed[i] = make([]bool, len(p[0]))
		}
	}
	if len(oldFlashed) == 0 {
		oldFlashed = make([][]bool, len(p))
		for i := range p {
			oldFlashed[i] = make([]bool, len(p[0]))
		}
	}
	var flashes int

	for flashedRow := range alreadyFlashed {
		for flashedColumn, v := range alreadyFlashed[flashedRow] {
			oldFlashed[flashedRow][flashedColumn] = v
		}
	}
	for row := range newP {
		for column := range newP[row] {
			if newP[row][column] > 9 && !alreadyFlashed[row][column] {
				alreadyFlashed[row][column] = true
				flashes++

				left, right := coord{row: row, column: column - 1}, coord{row: row, column: column + 1}
				up, down := coord{row: row - 1, column: column}, coord{row: row + 1, column: column}
				topLeft, topRight := coord{row: row - 1, column: column - 1}, coord{row: row - 1, column: column + 1}
				bottomLeft, bottomRight := coord{row: row + 1, column: column - 1}, coord{row: row + 1, column: column + 1}
				for _, co := range []coord{left, right, up, down, topLeft, topRight, bottomLeft, bottomRight} {
					if co.row < 0 || co.row > (len(p)-1) || co.column < 0 || co.column > (len(p[0])-1) {
						continue
					}
					newP[co.row][co.column]++
				}
			}
		}
	}
	for flashedRow := range oldFlashed {
		for flashedColumn, v := range oldFlashed[flashedRow] {
			if alreadyFlashed[flashedRow][flashedColumn] != v {
				var otherFlashes int
				newP, otherFlashes = p.flash(newP, alreadyFlashed)
				return newP, otherFlashes + flashes
			}
		}
	}
	return newP, flashes

}

func (p *population) step(steps int) int {
	var flashes int
	for i := 0; i < steps; i++ {
		p.increment()
		var newFlashes int
		*p, newFlashes = p.flash([][]uint8{}, [][]bool{})
		flashes += newFlashes
		p.resetFlash()
	}
	return flashes
}

func (p *population) synchronise() int {
	var flashes int
	var steps int
	for flashes != p.size() {
		p.increment()
		*p, flashes = p.flash([][]uint8{}, [][]bool{})

		p.resetFlash()
		steps++
	}
	return steps
}

func main() {
	contents, err := os.ReadFile("input_data.txt")
	if err != nil {
		panic(err)
	}
	var octos population = population{}
	octos.create(strings.Split(string(contents), "\n"))
	// fmt.Printf("The number of flashes after %d steps is %d\n", 100, octos.step(100))
	fmt.Printf("The octopi synchronise after %d steps\n", octos.synchronise())
}
