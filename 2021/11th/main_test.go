package main

import (
	"reflect"
	"testing"
)

func Test_population_create(t *testing.T) {
	type args struct {
		lines []string
	}
	tests := []struct {
		name string
		p    *population
		args args
		want *population
	}{
		{
			name: "Test Provided",
			p:    &population{},
			args: args{
				lines: []string{
					"5483143223",
					"2745854711",
					"5264556173",
					"6141336146",
					"6357385478",
					"4167524645",
					"2176821721",
					"6882881134",
					"4846848554",
					"5283751526",
				},
			},
			want: &population{
				{5, 4, 8, 3, 1, 4, 3, 2, 2, 3},
				{2, 7, 4, 5, 8, 5, 4, 7, 1, 1},
				{5, 2, 6, 4, 5, 5, 6, 1, 7, 3},
				{6, 1, 4, 1, 3, 3, 6, 1, 4, 6},
				{6, 3, 5, 7, 3, 8, 5, 4, 7, 8},
				{4, 1, 6, 7, 5, 2, 4, 6, 4, 5},
				{2, 1, 7, 6, 8, 2, 1, 7, 2, 1},
				{6, 8, 8, 2, 8, 8, 1, 1, 3, 4},
				{4, 8, 4, 6, 8, 4, 8, 5, 5, 4},
				{5, 2, 8, 3, 7, 5, 1, 5, 2, 6},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.p.create(tt.args.lines)
			for i := range *tt.p {
				for j, v := range (*tt.p)[i] {
					if v != (*tt.want)[i][j] {

						t.Errorf("population{}.create() = %v, want %v", tt.p, tt.want)
					}
				}
			}
		})
	}
}

func Test_population_increment(t *testing.T) {
	tests := []struct {
		name string
		p    *population
		want *population
	}{
		{
			name: "0s to 1s",
			p: &population{
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			},
			want: &population{
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.p.increment()
			for i := range *tt.p {
				for j, v := range (*tt.p)[i] {
					if v != (*tt.want)[i][j] {
						t.Errorf("population{}.increment() = %v, want %v", *tt.p, *tt.want)
					}
				}
			}
		})
	}
}

func Test_population_resetFlash(t *testing.T) {
	tests := []struct {
		name    string
		p, want *population
	}{
		{
			name: "0s to 1s",
			p: &population{
				{1, 2, 3, 4, 5, 6, 9, 7, 8, 0},
				{1, 2, 3, 12, 5, 6, 4, 7, 8, 0},
			},
			want: &population{
				{1, 2, 3, 4, 5, 6, 0, 7, 8, 0},
				{1, 2, 3, 0, 5, 6, 4, 7, 8, 0},
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.p.resetFlash()
			for i := range *tt.p {
				for j, v := range (*tt.p)[i] {
					wanted := (*tt.want)[i][j]
					if v != wanted {

						t.Errorf("population.resetFlash() = %v, want %v", *tt.p, *tt.want)
					}
				}
			}
		})
	}
}

func Test_population_flash(t *testing.T) {
	type args struct {
		newP           population
		alreadyFlashed [][]bool
	}
	tests := []struct {
		name        string
		p           population
		args        args
		want        population
		wantFlashes int
	}{
		{
			name: "Small Test Provided - Step 1",
			p: population{
				{2, 2, 2, 2, 2},
				{2, 10, 10, 10, 2},
				{2, 10, 2, 10, 2},
				{2, 10, 10, 10, 2},
				{2, 2, 2, 2, 2},
			},
			want: population{
				{3, 4, 5, 4, 3},
				{4, 13, 15, 13, 4},
				{5, 15, 10, 15, 5},
				{4, 13, 15, 13, 4},
				{3, 4, 5, 4, 3},
			},
			wantFlashes: 9,
			args: args{
				newP:           population{},
				alreadyFlashed: [][]bool{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, gotFlashes := tt.p.flash(tt.args.newP, tt.args.alreadyFlashed)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("population.flash() = %v, want %v", got, tt.want)
			}
			if gotFlashes != tt.wantFlashes {
				t.Errorf("population.flash() = _, %v, want %v", gotFlashes, tt.wantFlashes)
			}
		})
	}
}

func Test_population_step(t *testing.T) {
	tests := []struct {
		name        string
		p           *population
		want        *population
		wantFlashes int
		steps       int
	}{
		{
			name: "Small Test Provided - Step 1",
			p: &population{
				{1, 1, 1, 1, 1},
				{1, 9, 9, 9, 1},
				{1, 9, 1, 9, 1},
				{1, 9, 9, 9, 1},
				{1, 1, 1, 1, 1},
			},
			want: &population{
				{3, 4, 5, 4, 3},
				{4, 0, 0, 0, 4},
				{5, 0, 0, 0, 5},
				{4, 0, 0, 0, 4},
				{3, 4, 5, 4, 3},
			},
			steps:       1,
			wantFlashes: 9,
		},
		{
			name: "Small Test Provided - Step 2",
			p: &population{
				{1, 1, 1, 1, 1},
				{1, 9, 9, 9, 1},
				{1, 9, 1, 9, 1},
				{1, 9, 9, 9, 1},
				{1, 1, 1, 1, 1},
			},
			want: &population{
				{4, 5, 6, 5, 4},
				{5, 1, 1, 1, 5},
				{6, 1, 1, 1, 6},
				{5, 1, 1, 1, 5},
				{4, 5, 6, 5, 4},
			},
			steps:       2,
			wantFlashes: 9,
		},
		{
			name: "Big Test Provided - Step 1",
			p: &population{
				{5, 4, 8, 3, 1, 4, 3, 2, 2, 3},
				{2, 7, 4, 5, 8, 5, 4, 7, 1, 1},
				{5, 2, 6, 4, 5, 5, 6, 1, 7, 3},
				{6, 1, 4, 1, 3, 3, 6, 1, 4, 6},
				{6, 3, 5, 7, 3, 8, 5, 4, 7, 8},
				{4, 1, 6, 7, 5, 2, 4, 6, 4, 5},
				{2, 1, 7, 6, 8, 4, 1, 7, 2, 1},
				{6, 8, 8, 2, 8, 8, 1, 1, 3, 4},
				{4, 8, 4, 6, 8, 4, 8, 5, 5, 4},
				{5, 2, 8, 3, 7, 5, 1, 5, 2, 6},
			},
			want: &population{
				{6, 5, 9, 4, 2, 5, 4, 3, 3, 4},
				{3, 8, 5, 6, 9, 6, 5, 8, 2, 2},
				{6, 3, 7, 5, 6, 6, 7, 2, 8, 4},
				{7, 2, 5, 2, 4, 4, 7, 2, 5, 7},
				{7, 4, 6, 8, 4, 9, 6, 5, 8, 9},
				{5, 2, 7, 8, 6, 3, 5, 7, 5, 6},
				{3, 2, 8, 7, 9, 5, 2, 8, 3, 2},
				{7, 9, 9, 3, 9, 9, 2, 2, 4, 5},
				{5, 9, 5, 7, 9, 5, 9, 6, 6, 5},
				{6, 3, 9, 4, 8, 6, 2, 6, 3, 7},
			},
			steps:       1,
			wantFlashes: 0,
		},
		{
			name: "Big Test Provided - Step 2",
			p: &population{
				{6, 5, 9, 4, 2, 5, 4, 3, 3, 4},
				{3, 8, 5, 6, 9, 6, 5, 8, 2, 2},
				{6, 3, 7, 5, 6, 6, 7, 2, 8, 4},
				{7, 2, 5, 2, 4, 4, 7, 2, 5, 7},
				{7, 4, 6, 8, 4, 9, 6, 5, 8, 9},
				{5, 2, 7, 8, 6, 3, 5, 7, 5, 6},
				{3, 2, 8, 7, 9, 5, 2, 8, 3, 2},
				{7, 9, 9, 3, 9, 9, 2, 2, 4, 5},
				{5, 9, 5, 7, 9, 5, 9, 6, 6, 5},
				{6, 3, 9, 4, 8, 6, 2, 6, 3, 7},
			},
			want: &population{
				{8, 8, 0, 7, 4, 7, 6, 5, 5, 5},
				{5, 0, 8, 9, 0, 8, 7, 0, 5, 4},
				{8, 5, 9, 7, 8, 8, 9, 6, 0, 8},
				{8, 4, 8, 5, 7, 6, 9, 6, 0, 0},
				{8, 7, 0, 0, 9, 0, 8, 8, 0, 0},
				{6, 6, 0, 0, 0, 8, 8, 9, 8, 9},
				{6, 8, 0, 0, 0, 0, 5, 9, 4, 3},
				{0, 0, 0, 0, 0, 0, 7, 4, 5, 6},
				{9, 0, 0, 0, 0, 0, 0, 8, 7, 6},
				{8, 7, 0, 0, 0, 0, 6, 8, 4, 8},
			},
			steps:       1,
			wantFlashes: 35,
		},
		{
			name: "Big Test Provided - Step 3",
			p: &population{
				{8, 8, 0, 7, 4, 7, 6, 5, 5, 5},
				{5, 0, 8, 9, 0, 8, 7, 0, 5, 4},
				{8, 5, 9, 7, 8, 8, 9, 6, 0, 8},
				{8, 4, 8, 5, 7, 6, 9, 6, 0, 0},
				{8, 7, 0, 0, 9, 0, 8, 8, 0, 0},
				{6, 6, 0, 0, 0, 8, 8, 9, 8, 9},
				{6, 8, 0, 0, 0, 0, 5, 9, 4, 3},
				{0, 0, 0, 0, 0, 0, 7, 4, 5, 6},
				{9, 0, 0, 0, 0, 0, 0, 8, 7, 6},
				{8, 7, 0, 0, 0, 0, 6, 8, 4, 8},
			},
			want: &population{
				{0, 0, 5, 0, 9, 0, 0, 8, 6, 6},
				{8, 5, 0, 0, 8, 0, 0, 5, 7, 5},
				{9, 9, 0, 0, 0, 0, 0, 0, 3, 9},
				{9, 7, 0, 0, 0, 0, 0, 0, 4, 1},
				{9, 9, 3, 5, 0, 8, 0, 0, 6, 3},
				{7, 7, 1, 2, 3, 0, 0, 0, 0, 0},
				{7, 9, 1, 1, 2, 5, 0, 0, 0, 9},
				{2, 2, 1, 1, 1, 3, 0, 0, 0, 0},
				{0, 4, 2, 1, 1, 2, 5, 0, 0, 0},
				{0, 0, 2, 1, 1, 1, 9, 0, 0, 0},
			},
			steps:       1,
			wantFlashes: 45,
		},
		{
			name: "Big Test Provided - Step 0 to Step 3",
			p: &population{
				{5, 4, 8, 3, 1, 4, 3, 2, 2, 3},
				{2, 7, 4, 5, 8, 5, 4, 7, 1, 1},
				{5, 2, 6, 4, 5, 5, 6, 1, 7, 3},
				{6, 1, 4, 1, 3, 3, 6, 1, 4, 6},
				{6, 3, 5, 7, 3, 8, 5, 4, 7, 8},
				{4, 1, 6, 7, 5, 2, 4, 6, 4, 5},
				{2, 1, 7, 6, 8, 4, 1, 7, 2, 1},
				{6, 8, 8, 2, 8, 8, 1, 1, 3, 4},
				{4, 8, 4, 6, 8, 4, 8, 5, 5, 4},
				{5, 2, 8, 3, 7, 5, 1, 5, 2, 6},
			},
			want: &population{
				{0, 0, 5, 0, 9, 0, 0, 8, 6, 6},
				{8, 5, 0, 0, 8, 0, 0, 5, 7, 5},
				{9, 9, 0, 0, 0, 0, 0, 0, 3, 9},
				{9, 7, 0, 0, 0, 0, 0, 0, 4, 1},
				{9, 9, 3, 5, 0, 8, 0, 0, 6, 3},
				{7, 7, 1, 2, 3, 0, 0, 0, 0, 0},
				{7, 9, 1, 1, 2, 5, 0, 0, 0, 9},
				{2, 2, 1, 1, 1, 3, 0, 0, 0, 0},
				{0, 4, 2, 1, 1, 2, 5, 0, 0, 0},
				{0, 0, 2, 1, 1, 1, 9, 0, 0, 0},
			},
			steps:       3,
			wantFlashes: 80,
		},
		{
			name: "Big Test Provided - Step 4",
			p: &population{
				{0, 0, 5, 0, 9, 0, 0, 8, 6, 6},
				{8, 5, 0, 0, 8, 0, 0, 5, 7, 5},
				{9, 9, 0, 0, 0, 0, 0, 0, 3, 9},
				{9, 7, 0, 0, 0, 0, 0, 0, 4, 1},
				{9, 9, 3, 5, 0, 8, 0, 0, 6, 3},
				{7, 7, 1, 2, 3, 0, 0, 0, 0, 0},
				{7, 9, 1, 1, 2, 5, 0, 0, 0, 9},
				{2, 2, 1, 1, 1, 3, 0, 0, 0, 0},
				{0, 4, 2, 1, 1, 2, 5, 0, 0, 0},
				{0, 0, 2, 1, 1, 1, 9, 0, 0, 0},
			},
			want: &population{
				{2, 2, 6, 3, 0, 3, 1, 9, 7, 7},
				{0, 9, 2, 3, 0, 3, 1, 6, 9, 7},
				{0, 0, 3, 2, 2, 2, 1, 1, 5, 0},
				{0, 0, 4, 1, 1, 1, 1, 1, 6, 3},
				{0, 0, 7, 6, 1, 9, 1, 1, 7, 4},
				{0, 0, 5, 3, 4, 1, 1, 1, 2, 2},
				{0, 0, 4, 2, 3, 6, 1, 1, 2, 0},
				{5, 5, 3, 2, 2, 4, 1, 1, 2, 2},
				{1, 5, 3, 2, 2, 4, 7, 2, 1, 1},
				{1, 1, 3, 2, 2, 3, 0, 2, 1, 1},
			},
			steps:       1,
			wantFlashes: 16,
		},
		{
			name: "Big Test Provided - Step 1 - Step 10",
			p: &population{
				{5, 4, 8, 3, 1, 4, 3, 2, 2, 3},
				{2, 7, 4, 5, 8, 5, 4, 7, 1, 1},
				{5, 2, 6, 4, 5, 5, 6, 1, 7, 3},
				{6, 1, 4, 1, 3, 3, 6, 1, 4, 6},
				{6, 3, 5, 7, 3, 8, 5, 4, 7, 8},
				{4, 1, 6, 7, 5, 2, 4, 6, 4, 5},
				{2, 1, 7, 6, 8, 4, 1, 7, 2, 1},
				{6, 8, 8, 2, 8, 8, 1, 1, 3, 4},
				{4, 8, 4, 6, 8, 4, 8, 5, 5, 4},
				{5, 2, 8, 3, 7, 5, 1, 5, 2, 6},
			},
			want: &population{
				{0, 4, 8, 1, 1, 1, 2, 9, 7, 6},
				{0, 0, 3, 1, 1, 1, 2, 0, 0, 9},
				{0, 0, 4, 1, 1, 1, 2, 5, 0, 4},
				{0, 0, 8, 1, 1, 1, 1, 4, 0, 6},
				{0, 0, 9, 9, 1, 1, 1, 3, 0, 6},
				{0, 0, 9, 3, 5, 1, 1, 2, 3, 3},
				{0, 4, 4, 2, 3, 6, 1, 1, 3, 0},
				{5, 5, 3, 2, 2, 5, 2, 3, 5, 0},
				{0, 5, 3, 2, 2, 5, 0, 6, 0, 0},
				{0, 0, 3, 2, 2, 4, 0, 0, 0, 0},
			},
			steps:       10,
			wantFlashes: 204,
		},
		{
			name: "Big Test Provided - Step 100",
			p: &population{
				{5, 4, 8, 3, 1, 4, 3, 2, 2, 3},
				{2, 7, 4, 5, 8, 5, 4, 7, 1, 1},
				{5, 2, 6, 4, 5, 5, 6, 1, 7, 3},
				{6, 1, 4, 1, 3, 3, 6, 1, 4, 6},
				{6, 3, 5, 7, 3, 8, 5, 4, 7, 8},
				{4, 1, 6, 7, 5, 2, 4, 6, 4, 5},
				{2, 1, 7, 6, 8, 4, 1, 7, 2, 1},
				{6, 8, 8, 2, 8, 8, 1, 1, 3, 4},
				{4, 8, 4, 6, 8, 4, 8, 5, 5, 4},
				{5, 2, 8, 3, 7, 5, 1, 5, 2, 6},
			},
			want: &population{
				{0, 3, 9, 7, 6, 6, 6, 8, 6, 6},
				{0, 7, 4, 9, 7, 6, 6, 9, 1, 8},
				{0, 0, 5, 3, 9, 7, 6, 9, 3, 3},
				{0, 0, 0, 4, 2, 9, 7, 8, 2, 2},
				{0, 0, 0, 4, 2, 2, 9, 8, 9, 2},
				{0, 0, 5, 3, 2, 2, 2, 8, 7, 7},
				{0, 5, 3, 2, 2, 2, 2, 9, 6, 6},
				{9, 3, 2, 2, 2, 2, 8, 9, 6, 6},
				{7, 9, 2, 2, 2, 8, 6, 8, 6, 6},
				{6, 7, 8, 9, 9, 9, 8, 7, 6, 6},
			},
			steps:       100,
			wantFlashes: 1656,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotFlashes := tt.p.step(tt.steps)
			for i := range *tt.p {
				for j, v := range (*tt.p)[i] {
					wanted := (*tt.want)[i][j]
					if v != wanted {

						t.Errorf("population.step()[%d][%d] = %v, want %v", i, j, v, wanted)
					}
				}
			}
			if gotFlashes != tt.wantFlashes {
				t.Errorf("population.step() = _, %v, want %v", gotFlashes, tt.wantFlashes)
			}
		})
	}
}
