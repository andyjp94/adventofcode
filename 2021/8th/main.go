package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"math"
	"reflect"
	"sort"
	"strings"
)

// These are used to track the number of segments
// required to make each number, I'll be the first
// to admit this is confusing.
const (
	ONE   int = 2
	FOUR  int = 4
	SEVEN int = 3
	EIGHT int = 7
	ZERO  int = 6
	SIX   int = 6
)

type digit map[byte]bool
type digits []digit

func (d digit) String() string {
	var w string
	for k := range d {
		w += string(k)
	}
	// Alphabetical sort
	s := strings.Split(w, "")
	sort.Strings(s)
	return fmt.Sprintf("|components: %s|\n", strings.Join(s, ""))
}

func (d digit) create(components string) {

	for _, c := range components {
		d[byte(c)] = true
	}
}
func (d digit) contains(otherD digit) bool {
	for v := range otherD {
		if _, contains := d[v]; !contains {
			return false
		}
	}
	return true
}

func getIO(line string) (inputs []string, outputs []string) {
	separated_values := strings.Split(string(line), "|")
	inputs = strings.Fields(separated_values[0])
	outputs = strings.Fields(separated_values[1])

	return
}

func findSegmentC(one, six digit) (byte, error) {
	for val := range one {
		if _, contains := six[val]; !contains {
			return val, nil
		}
	}
	return byte('x'), errors.New("segment C could not be found")
}

func findDigits(values []string) (ds digits) {
	for len(ds) < 10 {
	out:
		for _, v := range values {
			var d digit = digit{}
			d.create(v)
			var alreadyExists bool
			for _, m := range ds {
				if reflect.DeepEqual(d, m) {
					alreadyExists = true
					break
				}
			}
			if !alreadyExists {
				ds = append(ds, d)
				break out
			}
		}
	}
	return ds
}
func separateUniqueLenDigits(ds digits) (map[int]digit, digits) {
	var nonUnique digits
	var unique map[int]digit = make(map[int]digit)
	for _, v := range ds {
		switch len(v) {
		case ONE:
			unique[1] = v
		case FOUR:
			unique[4] = v
		case SEVEN:
			unique[7] = v
		case EIGHT:
			unique[8] = v
		default:
			nonUnique = append(nonUnique, v)

		}
	}
	return unique, nonUnique
}

func (ds *digits) popDigitSix(one digit) digit {
	for i, d := range *ds {
		// The only digits that are made up of 6 segments are 0, 6 and 9. Only
		// 6 does not contain the segments required to create 1
		if len(d) == 6 && !d.contains(one) {
			*ds = append((*ds)[:i], (*ds)[i+1:]...)
			return d
		}
	}
	return nil
}

func (ds *digits) popDigitZero(four digit) digit {
	for i, d := range *ds {
		// The only digits that are made up of 6 segments are 0, 6 and 9. When
		// this function is called we already know 6. Of the two remaining only
		// 0 does not contiain all the values in 4
		if len(d) == 6 && !d.contains(four) {
			*ds = append((*ds)[:i], (*ds)[i+1:]...)
			return d
		}
	}
	return nil
}
func (ds *digits) popDigitNine() digit {
	// This only runs after 0 and 6 have been created
	for i, d := range *ds {
		if len(d) == 6 {
			*ds = append((*ds)[:i], (*ds)[i+1:]...)
			return d
		}
	}
	return nil
}

func (ds *digits) popDigitThree(one digit) digit {

	for i, d := range *ds {
		if len(d) == 5 && d.contains(one) {
			*ds = append((*ds)[:i], (*ds)[i+1:]...)
			return d
		}
	}
	return nil
}

func (ds *digits) popDigitTwo(c byte) digit {
	// Runs after 3 has been found
	for i, d := range *ds {
		if len(d) == 5 {
			if _, contains := d[c]; contains {
				*ds = append((*ds)[:i], (*ds)[i+1:]...)
				return d
			}
		}
	}
	return nil
}

func main() {
	contents, err := ioutil.ReadFile("input_data.txt")
	if err != nil {
		panic(err)
	}

	var totalOutput float64
	for _, line := range strings.Split(string(contents), "\n") {
		inputs, outputs := getIO(string(line))
		ds := findDigits(inputs)
		// organisedDigits, _ := separateUniqueLenDigits(digits)
		organisedDigits, nonUniqueLenDigits := separateUniqueLenDigits(ds)
		var segments map[byte]byte = make(map[byte]byte)
		// At this point we know what 1, 4, 7 and 8 are
		organisedDigits[6] = nonUniqueLenDigits.popDigitSix(organisedDigits[1])

		// Now that we know 1 and we know 6 we can work out which segment is c
		segments[byte('c')], err = findSegmentC(organisedDigits[1], organisedDigits[6])
		if err != nil {
			panic(err)
		}
		organisedDigits[0] = nonUniqueLenDigits.popDigitZero(organisedDigits[4])
		organisedDigits[9] = nonUniqueLenDigits.popDigitNine()

		organisedDigits[3] = nonUniqueLenDigits.popDigitThree(organisedDigits[1])
		organisedDigits[2] = nonUniqueLenDigits.popDigitTwo(segments[byte('c')])
		// The only one left
		organisedDigits[5] = nonUniqueLenDigits[0]

		// We now know all the digits
		var total float64
		for i, output := range outputs {
			var newDigit digit = digit{}
			newDigit.create(output)
			for j, d := range organisedDigits {
				if len(d) == len(newDigit) && d.contains(newDigit) {
					power := float64(len(outputs) - i - 1)
					total += float64(j) * math.Pow(10, power)
				}
			}
		}
		totalOutput += total
	}
	fmt.Println(totalOutput)

}
