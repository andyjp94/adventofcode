package main

import (
	"reflect"
	"testing"
)

func Test_findCorruptions(t *testing.T) {
	type args struct {
		lines []string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test Provided",
			args: args{
				lines: []string{
					"[({(<(())[]>[[{[]{<()<>>",
					"[(()[<>])]({[<{<<[]>>(",
					"{([(<{}[<>[]}>{[]{[(<()>",
					"(((({<>}<{<{<>}{[]{[]{}",
					"[[<[([]))<([[{}[[()]]]",
					"[{[{({}]{}}([{[{{{}}([]",
					"{<[[]]>}<{[{[{[]{()[[[]",
					"[<(<(<(<{}))><([]([]()",
					"<{([([[(<>()){}]>(<<{{",
					"<{([{{}}[<[[[<>{}]]]>[]]",
				},
			},
			want: 26397,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := countCorruptions(tt.args.lines); got != tt.want {
				t.Errorf("countCorruptions() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_findCorruption(t *testing.T) {
	type args struct {
		line string
	}
	tests := []struct {
		name string
		args args
		want rune
	}{
		{
			name: "Test Provided - One",
			args: args{
				line: "{([(<{}[<>[]}>{[]{[(<()>",
			},
			want: rune('}'),
		},
		{
			name: "Test Provided - Two",
			args: args{
				line: "[[<[([]))<([[{}[[()]]]",
			},
			want: rune(')'),
		},
		{
			name: "Test Provided - Three",
			args: args{
				line: "[{[{({}]{}}([{[{{{}}([]",
			},
			want: rune(']'),
		},
		{
			name: "Test Provided - Four",
			args: args{
				line: "[<(<(<(<{}))><([]([]()",
			},
			want: rune(')'),
		},
		{
			name: "Test Provided - Five",
			args: args{
				line: "<{([([[(<>()){}]>(<<{{",
			},
			want: rune('>'),
		},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findCorruption(tt.args.line); got != tt.want {
				t.Errorf("findCorruption() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_findIncompleteEndings(t *testing.T) {
	type args struct {
		line string
	}
	tests := []struct {
		name string
		args args
		want []rune
	}{
		{
			name: "Test Provided - One",
			args: args{
				line: "[({(<(())[]>[[{[]{<()<>>",
			},
			want: []rune{'}', '}', ']', ']', ')', '}', ')', ']'},
		},
		{
			name: "Test Provided - Two",
			args: args{
				line: "[(()[<>])]({[<{<<[]>>(",
			},
			want: []rune{')', '}', '>', ']', '}', ')'},
		},

		{
			name: "Test Provided - Three",
			args: args{
				line: "(((({<>}<{<{<>}{[]{[]{}",
			},
			want: []rune{'}', '}', '>', '}', '>', ')', ')', ')', ')'},
		},
		{
			name: "Test Provided - Four",
			args: args{
				line: "{<[[]]>}<{[{[{[]{()[[[]",
			},
			want: []rune{']', ']', '}', '}', ']', '}', ']', '}', '>'},
		},
		{
			name: "Test Provided - Five",
			args: args{
				line: "<{([{{}}[<[[[<>{}]]]>[]]",
			},
			want: []rune{']', ')', '}', '>'},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findIncompleteEndings(tt.args.line); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("findIncompleteEndings() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_scoreIncompleteLine(t *testing.T) {
	type args struct {
		line string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test Provided - One",
			args: args{
				line: "[({(<(())[]>[[{[]{<()<>>",
			},
			want: 288957,
		},
		{
			name: "Test Provided - Two",
			args: args{
				line: "[(()[<>])]({[<{<<[]>>(",
			},
			want: 5566,
		},
		{
			name: "Test Provided - Three",
			args: args{
				line: "(((({<>}<{<{<>}{[]{[]{}",
			},
			want: 1480781,
		},
		{
			name: "Test Provided - Four",
			args: args{
				line: "{<[[]]>}<{[{[{[]{()[[[]",
			},
			want: 995444,
		},
		{
			name: "Test Provided - Five",
			args: args{
				line: "<{([{{}}[<[[[<>{}]]]>[]]",
			},
			want: 294,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := scoreIncompleteLine(tt.args.line); got != tt.want {
				t.Errorf("scoreIncompleteLine() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_findIncompleteWinner(t *testing.T) {
	type args struct {
		lines []string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test Provided",
			args: args{
				lines: []string{
					"[({(<(())[]>[[{[]{<()<>>",
					"[(()[<>])]({[<{<<[]>>(",
					"{([(<{}[<>[]}>{[]{[(<()>",
					"(((({<>}<{<{<>}{[]{[]{}",
					"[[<[([]))<([[{}[[()]]]",
					"[{[{({}]{}}([{[{{{}}([]",
					"{<[[]]>}<{[{[{[]{()[[[]",
					"[<(<(<(<{}))><([]([]()",
					"<{([([[(<>()){}]>(<<{{",
					"<{([{{}}[<[[[<>{}]]]>[]]",
				},
			},
			want: 288957,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findIncompleteWinner(tt.args.lines); got != tt.want {
				t.Errorf("findIncompleteWinner() = %v, want %v", got, tt.want)
			}
		})
	}
}
