package main

import (
	"fmt"
	"os"
	"sort"
	"strings"
)

var chunkDelimiters map[rune]rune = map[rune]rune{
	rune('['): rune(']'),
	rune('{'): rune('}'),
	rune('('): rune(')'),
	rune('<'): rune('>'),
}
var delimiterValues map[rune]int = map[rune]int{
	rune(']'): 57,
	rune('}'): 1197,
	rune(')'): 3,
	rune('>'): 25137,
}

var autoCompleteValues map[rune]int = map[rune]int{
	rune(']'): 2,
	rune('}'): 3,
	rune(')'): 1,
	rune('>'): 4,
}

func findIncompleteEndings(line string) []rune {
	// chunkOpener acts as a FILO queue
	// to store all the openers that have yet to be closed
	var chunkOpener []rune
	for _, r := range line {
		switch r {
		case rune('['), rune('{'), rune('('), rune('<'):
			chunkOpener = append(chunkOpener, r)
		case rune(']'), rune('}'), rune(')'), rune('>'):
			matchingCloser := chunkDelimiters[chunkOpener[len(chunkOpener)-1]]
			if r != matchingCloser {
				return []rune{}
			} else {
				chunkOpener = chunkOpener[:len(chunkOpener)-1]
			}
		}
	}
	var chunkClosers []rune
	for i := len(chunkOpener) - 1; i >= 0; i-- {
		chunkClosers = append(chunkClosers, chunkDelimiters[chunkOpener[i]])
	}
	return chunkClosers
}

func scoreIncompleteLine(line string) int {
	endings := findIncompleteEndings(line)
	if len(endings) == 0 {
		return 0
	}
	var sum int
	for _, v := range endings {
		sum = sum*5 + autoCompleteValues[v]
	}
	return sum
}
func findIncompleteWinner(lines []string) int {
	var scores []int
	for _, line := range lines {
		score := scoreIncompleteLine(line)
		if score != 0 {
			scores = append(scores, score)
		}

	}
	sort.Ints(scores)
	return scores[len(scores)/2]
}

func findCorruption(line string) rune {
	// chunkOpener acts as a FILO queue
	// to store all the openers that have yet to be closed
	var chunkOpener []rune
	for _, r := range line {
		switch r {
		case rune('['), rune('{'), rune('('), rune('<'):
			chunkOpener = append(chunkOpener, r)
		case rune(']'), rune('}'), rune(')'), rune('>'):
			matchingCloser := chunkDelimiters[chunkOpener[len(chunkOpener)-1]]
			if r != matchingCloser {
				return r
			} else {
				chunkOpener = chunkOpener[:len(chunkOpener)-1]
			}
		}
	}
	return rune(0)
}
func countCorruptions(lines []string) int {
	var sum int
	for _, line := range lines {

		r := findCorruption(line)
		if r != rune(0) {
			sum += delimiterValues[r]
		}
	}
	return sum
}

func main() {

	contents, err := os.ReadFile("input_data.txt")
	if err != nil {
		panic(err)
	}
	fmt.Printf("The total value of the corruptions is: %d.\n", countCorruptions(strings.Split(string(contents), "\n")))
	fmt.Printf("The autocomplete winner is: %d.\n", findIncompleteWinner(strings.Split(string(contents), "\n")))
}
