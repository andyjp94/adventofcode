package main

import (
	"testing"
)

func Test_countDepthIncreases(t *testing.T) {
	type args struct {
		scanner []string
		window  int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Provided example",
			args: args{
				scanner: []string{"199", "200", "208", "210", "200", "207", "240", "269", "260", "263"},
				window:  3,
			},
			want: 5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := countDepthIncreases(tt.args.scanner, tt.args.window); got != tt.want {
				t.Errorf("countDepthIncreases() = %v, want %v", got, tt.want)
			}
		})
	}
}
