package main

import (
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

// Assumes a slice of strings contains integers
func sumSlice(s []string, start, stop int) int {
	var sum int
	for _, v := range s[start:stop] {
		currentVal, err := strconv.Atoi(v)
		if err != nil {
			log.Fatal(err)
		}
		sum += currentVal
	}
	return sum
}

func countDepthIncreases(depths []string, window int) int {
	// -1 is a special state, if the value is -1 then it is ignored
	// var A, B []int
	var numDepthIncreases int

	for i := 0; i < len(depths); i++ {
		if i+1+window > len(depths) {
			return numDepthIncreases
		}
		var a_value, b_value int
		a_value = sumSlice(depths, i, i+window)
		b_value = sumSlice(depths, i+1, i+window+1)
		if b_value > a_value {
			numDepthIncreases++
		}
	}
	return numDepthIncreases
}

func main() {

	content, err := ioutil.ReadFile("input_data.txt")

	if err != nil {
		log.Fatal(err)
	}
	depths := strings.Split(string(content), "\n")

	log.Printf("The number of depth increases is %d", countDepthIncreases(depths, 3))
}
