package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func updateLocation(instructions []string) int {
	var position, depth, aim int
	for _, instruction := range instructions {
		instuction_components := strings.Split(instruction, " ")
		direction := instuction_components[0]
		magnitude, err := strconv.Atoi(instuction_components[1])
		if err != nil {
			log.Fatal(err)
		}
		switch direction {
		case "forward":
			position += magnitude
			depth += aim * magnitude
		case "down":
			aim += magnitude
		case "up":
			aim -= magnitude
		}
	}
	return position * depth
}

func main() {

	content, err := ioutil.ReadFile("input_data.txt")

	if err != nil {
		log.Fatal(err)
	}
	instructions := strings.Split(string(content), "\n")
	fmt.Println(updateLocation(instructions))

}
