package main

import "testing"

func Test_updateLocation(t *testing.T) {
	type args struct {
		instructions []string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Provided Test",
			args: args{
				instructions: []string{"forward 5", "down 5", "forward 8", "up 3", "down 8", "forward 2"},
			},
			want: 900,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := updateLocation(tt.args.instructions); got != tt.want {
				t.Errorf("updateLocation() = %v, want %v", got, tt.want)
			}
		})
	}
}
