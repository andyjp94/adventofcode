package main

import (
	"fmt"
	"reflect"
	"testing"
)

func Test_parseCoord(t *testing.T) {
	type args struct {
		line string
	}
	tests := []struct {
		name string
		args args
		want coord
	}{
		{
			name: "Basic coord",
			args: args{
				line: "0,1",
			},
			want: coord{x: 0, y: 1},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseCoord(tt.args.line); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseCoord() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_segment_createHorizontal(t *testing.T) {
	type fields struct {
		start     coord
		finish    coord
		direction Direction
	}
	type args struct {
		coords [2]coord
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "Basic Test",
			fields: fields{},
			args: args{
				[2]coord{{y: 4, x: 2}, {y: 4, x: 10}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &segment{
				start:     tt.fields.start,
				finish:    tt.fields.finish,
				direction: tt.fields.direction,
			}
			s.createHorizontal(tt.args.coords)
			if s.direction != HORIZONTAL {
				t.Errorf("segment.createHorizontal(coords) has set the wrong direction")
			}
			if s.finish.x != 10 {
				t.Errorf("segment.createHorizontal(coords) has set the wrong finishing point")
			}
		})
	}
}

func Test_segment_createVertical(t *testing.T) {
	type fields struct {
		start     coord
		finish    coord
		direction Direction
	}
	type args struct {
		coords [2]coord
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "Basic Test",
			fields: fields{},
			args: args{
				[2]coord{{y: 7, x: 2}, {y: 4, x: 2}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &segment{
				start:     tt.fields.start,
				finish:    tt.fields.finish,
				direction: tt.fields.direction,
			}
			s.createVertical(tt.args.coords)
			if s.direction != VERTICAL {
				t.Errorf("segment.createVertical(coords) has set the wrong direction")
			}
			if s.finish.y != 7 {
				t.Errorf("segment.createVertical(coords) has set the wrong finishing point")
			}
		})
	}
}

func Test_vent_map_drawSegment(t *testing.T) {
	type args struct {
		s segment
	}
	tests := []struct {
		name string
		m    *vent_map
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.m.drawSegment(tt.args.s)
		})
	}
}

func Test_segment_create(t *testing.T) {
	type fields struct {
		start     coord
		finish    coord
		direction Direction
	}
	type args struct {
		line string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Basic Test",
			args: args{
				line: "0,9 -> 5,9",
			},
			fields: fields{},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &segment{
				start:     tt.fields.start,
				finish:    tt.fields.finish,
				direction: tt.fields.direction,
			}
			s.create(tt.args.line)
			if s.start.x != 0 || s.finish.x != 5 || s.direction != HORIZONTAL {
				fmt.Errorf("segment.create(line) has not created the correct segment %v", s)
			}
		})
	}
}

func Test_vent_map_createMap(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name string
		m    *vent_map
		args args
	}{
		{
			name: "Basic Test",
			m:    &vent_map{},
			args: args{
				input: `1,1 -> 3,3`,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.m.createMap(tt.args.input)
			fmt.Println(*tt.m)
		})
	}
}
