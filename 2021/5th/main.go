package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"strconv"
	"strings"
)

const MAP_SIZE int = 1000

type Direction int

const (
	HORIZONTAL Direction = iota
	VERTICAL
	DIAGONAL
	IGNORE
)

type coord struct {
	x int
	y int
}
type segment struct {
	start     coord
	finish    coord
	direction Direction
}
type vent_map [MAP_SIZE][MAP_SIZE]int

func (m *vent_map) drawSegment(s segment) {
	switch s.direction {
	case HORIZONTAL:
		for i := s.start.x; i <= s.finish.x; i++ {
			m[s.start.y][i] += 1
		}
	case VERTICAL:
		for i := s.start.y; i <= s.finish.y; i++ {
			m[i][s.start.x] += 1
		}
	case DIAGONAL:
		pos := s.start
		for {
			m[pos.y][pos.x] += 1
			if pos.x == s.finish.x {
				break
			}
			if pos.x > s.finish.x {
				pos.x--
			} else {
				pos.x++
			}
			if pos.y > s.finish.y {
				pos.y--
			} else {
				pos.y++
			}

		}
	}
}

func (m *vent_map) countDangerousPoints(danger_level int) int {
	var total_danger_points int
	for _, column := range m {
		for _, point := range column {
			if point >= danger_level {
				total_danger_points++
			}
		}
	}
	return total_danger_points
}

func parseCoord(line string) coord {
	raw_coord := strings.Split(line, ",")
	x, err := strconv.Atoi(raw_coord[0])
	if err != nil {
		panic(err)
	}
	y, err := strconv.Atoi(raw_coord[1])
	if err != nil {
		panic(err)
	}
	return coord{x: x, y: y}
}
func (s *segment) createHorizontal(coords [2]coord) {
	s.direction = HORIZONTAL
	if coords[0].x > coords[1].x {
		s.start = coords[1]
		s.finish = coords[0]
	} else {
		s.start = coords[0]
		s.finish = coords[1]
	}
}
func (s *segment) createVertical(coords [2]coord) {
	s.direction = VERTICAL
	if coords[0].y > coords[1].y {
		s.start = coords[1]
		s.finish = coords[0]
	} else {
		s.start = coords[0]
		s.finish = coords[1]
	}
}

func (s *segment) create(line string) {
	x := strings.Fields(line)
	var coords [2]coord
	for i, c := range []string{x[0], x[2]} {
		coords[i] = parseCoord(c)
	}

	if coords[0].y == coords[1].y {
		s.createHorizontal(coords)
		return
	} else if coords[0].x == coords[1].x {
		s.createVertical(coords)
		return
	}
	gradient := (float32(coords[0].y - coords[1].y)) / float32((coords[0].x - coords[1].x))
	if math.Abs(float64(gradient)) == 1 {
		s.direction = DIAGONAL
		s.start = coords[0]
		s.finish = coords[1]
	} else {
		s.direction = IGNORE
	}
}

func (m *vent_map) createMap(input string) {
	for _, v := range strings.Split(input, "\n") {
		var s segment = segment{}
		s.create(v)
		m.drawSegment(s)
	}
}

func main() {
	content, err := ioutil.ReadFile("input_data.txt")
	if err != nil {
		log.Fatal(err)
	}
	var v_map vent_map = vent_map{}
	v_map.createMap(string(content))
	fmt.Printf("The dangerous points are: %d\n", v_map.countDangerousPoints(2))

}
