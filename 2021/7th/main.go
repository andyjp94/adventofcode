package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

type point struct {
	value int
	freq  int
}
type numberline []point

func (n numberline) String() string {
	var result string
	for _, v := range n {
		result += fmt.Sprintf("|value %d|frequency %d|\n", v.value, v.freq)
	}
	return result
}

func (n numberline) fuelUsage(loc int) (fuel int) {

	for _, v := range n {
		diff := int(math.Abs(float64(loc - v.value)))

		fuel += diff * v.freq

	}
	return
}

func (n numberline) fuelUsageNonConstant(loc int) (fuel int) {

	x := func(a int) int {
		if a <= 1 {
			return 0
		}
		var sum int
		for i := 0; i < a; i++ {
			sum += i
		}
		return sum
	}
	for _, v := range n {
		diff := int(math.Abs(float64(loc - v.value)))
		inconstistentUsage := x(diff) + diff

		fuel += inconstistentUsage * v.freq

	}
	return
}

func generateNumberLine(vals []int) numberline {
	var number_Line numberline = numberline{}
	var newNumberLine []point = number_Line
	for _, v := range vals {
		var found bool
		for i, j := range number_Line {
			if j.value == v {
				newNumberLine[i].freq++
				found = true
				break
			} else if v < j.value {
				if i == 0 {
					newNumberLine = append([]point{{value: v, freq: 1}}, newNumberLine...)
				} else {
					insert := append([]point{{value: v, freq: 1}}, newNumberLine[i:]...)
					newNumberLine = append(newNumberLine[:i], insert...)
				}
				found = true
				break
			}
		}
		if !found {
			newNumberLine = append(newNumberLine, point{value: v, freq: 1})
		}
		number_Line = newNumberLine
	}
	return number_Line
}
func (n numberline) calculateLowestFuelUsage() (fuel int) {
	lowest_value := n[0].value
	highest_value := n[len(n)-1].value
	var usage int = -1
	for i := lowest_value; i <= highest_value; i++ {
		if usage == -1 {
			usage = n.fuelUsageNonConstant(i)
		} else {
			newUsage := n.fuelUsageNonConstant(i)
			if usage > newUsage {
				usage = newUsage
			} else if usage == newUsage {
				fmt.Println("Did not expect this.")
			}
		}
	}
	return usage
}
func main() {
	content, err := ioutil.ReadFile("input_data.txt")
	if err != nil {
		fmt.Errorf("Could not read input file.\n")
	}
	var input []int
	for _, v := range strings.Split(string(content), ",") {
		newVal, err := strconv.Atoi(v)
		if err != nil {
			fmt.Errorf("Could not convert %v into an int.\n", v)
		}
		input = append(input, newVal)
	}
	nl := generateNumberLine(input)
	fmt.Printf("The lowest fuel usage is %d\n.", nl.calculateLowestFuelUsage())
}
