package main

import (
	"testing"
)

func Test_generateNumberLine(t *testing.T) {
	type args struct {
		vals []int
	}
	tests := []struct {
		name string
		args args
		want numberline
	}{
		{
			name: "Test Provided",
			args: args{
				vals: []int{16, 1, 2, 0, 4, 2, 7, 1, 2, 14},
			},
			want: numberline{
				{value: 0, freq: 1},
				{value: 1, freq: 2},
				{value: 2, freq: 3},
				{value: 4, freq: 1},
				{value: 7, freq: 1},
				{value: 14, freq: 1},
				{value: 16, freq: 1},
			},
		},
		{
			name: "Sample of Input",
			args: args{
				vals: []int{1101, 1, 29, 67, 1102, 0, 1, 65, 1008, 65, 35, 66, 1005},
			},
			want: numberline{
				{value: 0, freq: 1},
				{value: 1, freq: 2},
				{value: 29, freq: 1},
				{value: 35, freq: 1},
				{value: 65, freq: 2},
				{value: 66, freq: 1},
				{value: 67, freq: 1},
				{value: 1005, freq: 1},
				{value: 1008, freq: 1},
				{value: 1101, freq: 1},
				{value: 1102, freq: 1},
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := generateNumberLine(tt.args.vals)
			for i, v := range got {
				if v.value != tt.want[i].value || v.freq != tt.want[i].freq {
					t.Errorf("generateNumberLine() = %+v, want %+v", got, tt.want)
				}
			}
		})
	}
}

func Test_numberline_fuelUsage(t *testing.T) {
	type args struct {
		loc int
	}
	tests := []struct {
		name     string
		n        numberline
		args     args
		wantFuel int
	}{
		{
			name: "Test Provided",
			args: args{
				loc: 2,
			},
			n: []point{
				{value: 0, freq: 1},
				{value: 1, freq: 2},
				{value: 2, freq: 3},
				{value: 4, freq: 1},
				{value: 7, freq: 1},
				{value: 14, freq: 1},
				{value: 16, freq: 1},
			},

			wantFuel: 37,
		},
		{
			name: "Another Test Provided",
			args: args{
				loc: 3,
			},
			n: []point{
				{value: 0, freq: 1},
				{value: 1, freq: 2},
				{value: 2, freq: 3},
				{value: 4, freq: 1},
				{value: 7, freq: 1},
				{value: 14, freq: 1},
				{value: 16, freq: 1},
			},

			wantFuel: 39,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotFuel := tt.n.fuelUsage(tt.args.loc); gotFuel != tt.wantFuel {
				t.Errorf("numberline.fuelUsage() = %v, want %v", gotFuel, tt.wantFuel)
			}
		})
	}
}

func Test_numberline_calculateLowestFuelUsage(t *testing.T) {
	tests := []struct {
		name    string
		n       numberline
		wantPos int
	}{
		{
			name: "Test Provided",
			n: []point{
				{value: 0, freq: 1},
				{value: 1, freq: 2},
				{value: 2, freq: 3},
				{value: 4, freq: 1},
				{value: 7, freq: 1},
				{value: 14, freq: 1},
				{value: 16, freq: 1},
			},

			wantPos: 168,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotPos := tt.n.calculateLowestFuelUsage(); gotPos != tt.wantPos {
				t.Errorf("numberline.calculateLowestFuelUsage() = %v, want %v", gotPos, tt.wantPos)
			}
		})
	}
}

func Test_numberline_fuelUsageNonConstant(t *testing.T) {
	type args struct {
		loc int
	}
	tests := []struct {
		name     string
		n        numberline
		args     args
		wantFuel int
	}{
		{
			name: "Simple Test",
			args: args{loc: 5},
			n: []point{
				{value: 1, freq: 1},
			},

			wantFuel: 10,
		},
		{
			name: "Another Simple Test",
			args: args{loc: 5},
			n: []point{
				{value: 16, freq: 1},
			},

			wantFuel: 66,
		},
		{
			name: "Simple Test",
			args: args{loc: 5},
			n: []point{
				{value: 14, freq: 2},
			},

			wantFuel: 90,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotFuel := tt.n.fuelUsageNonConstant(tt.args.loc); gotFuel != tt.wantFuel {
				t.Errorf("numberline.fuelUsageNonConstant() = %v, want %v", gotFuel, tt.wantFuel)
			}
		})
	}
}
