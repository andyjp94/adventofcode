package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

type square struct {
	value int
	found bool
}

const BINGO_SIZE int = 5
const NO_BINGO int = -1

type group [BINGO_SIZE]square
type board [BINGO_SIZE]group

func (b *board) create(input string) {

	scanner := bufio.NewScanner(strings.NewReader(input))
	rowNum := 0

	for scanner.Scan() {
		line_scanner := bufio.NewScanner(strings.NewReader(scanner.Text()))
		line_scanner.Split(bufio.ScanWords)
		count := 0
		var row group
		for line_scanner.Scan() {
			newValue, err := strconv.Atoi(line_scanner.Text())
			if err != nil {
				panic(err)

			}
			row[count] = square{value: newValue}
			count++
		}
		b[rowNum] = row
		rowNum++
	}
}
func (g *group) mark(val int) {
	for i := 0; i < BINGO_SIZE; i++ {
		if g[i].found {
			continue
		}
		if g[i].value == val {
			g[i].found = true
		}
	}
}
func (b *board) mark(val int) {
	for i := 0; i < BINGO_SIZE; i++ {
		b[i].mark(val)
	}
}
func (g group) bingo() bool {
	for _, s := range g {
		if !s.found {
			return false
		}
	}
	return true
}
func (b board) bingo() bool {
	// Check Rows
	for _, g := range b {
		if g.bingo() {
			return true
		}
	}
	// Check Columns
	for i := 0; i < BINGO_SIZE; i++ {
		var column group
		for j := 0; j < BINGO_SIZE; j++ {
			column[j] = b[j][i]
		}
		if column.bingo() {
			return true
		}
	}
	return false
}
func (g group) sumUnmarked() int {
	var unmarked int
	for _, s := range g {
		if !s.found {
			unmarked += s.value
		}
	}
	return unmarked
}
func (b board) sumUnmarked() int {
	var unmarked int
	for _, g := range b {
		unmarked += g.sumUnmarked()
	}
	return unmarked
}
func (b board) calculateScore(val int) int {
	return b.sumUnmarked() * val
}

func (b *board) completeTurn(val int) int {
	b.mark(val)
	if b.bingo() {
		return b.calculateScore(val)
	} else {
		return NO_BINGO
	}
}
func playBingo(boards []board, inputs []int) (score int) {

	for _, input := range inputs {
		for i := 0; i < len(boards); i++ {
			score = boards[i].completeTurn(input)
			if score != NO_BINGO {
				return score
			}
		}
	}
	return NO_BINGO
}

func loseBingo(boards []board, inputs []int) (score int) {
	var b []board = boards
	for _, input := range inputs {

		for i := 0; i < len(b); i++ {
			score = b[i].completeTurn(input)
			if score != NO_BINGO {
				if len(b) == 1 {
					return score
				} else {
					b = append(b[:i], b[i+1:]...)
					i = -1
				}
			}
		}
	}
	return NO_BINGO
}

func splitByEmptyNewline(str string) []string {
	strNormalized := regexp.
		MustCompile("\r\n").
		ReplaceAllString(str, "\n")

	return regexp.
		MustCompile(`\n\s*\n`).
		Split(strNormalized, -1)

}
func parse_raw_input(filename string) (inputs []int, boards []board) {

	contents, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	separated_inputs := strings.SplitN(string(contents), "\n", 3)
	raw_inputs := separated_inputs[0]
	for _, raw_input := range strings.Split(raw_inputs, ",") {
		input, err := strconv.Atoi(raw_input)
		if err != nil {
			panic(err)
		}
		inputs = append(inputs, input)
	}
	raw_boards := separated_inputs[2]
	var b board
	less_raw_boards := splitByEmptyNewline(raw_boards)
	for _, raw_b := range less_raw_boards {
		b.create(raw_b)
		boards = append(boards, b)
	}
	return
}

func main() {

	inputs, boards := parse_raw_input("input_data.txt")
	fmt.Printf("Winning Score is: %d\n", playBingo(boards, inputs))
	fmt.Printf("Losing Score is: %d\n", loseBingo(boards, inputs))

}
