package main

import (
	"testing"
)

func Test_board_create(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name string
		b    *board
		args args
		want board
	}{
		{
			name: "Simple Board",
			b:    &board{},
			want: board{
				group{
					square{value: 22, found: false},
					square{value: 13, found: false},
					square{value: 17, found: false},
					square{value: 11, found: false},
					square{value: 0, found: false},
				},
				group{
					square{value: 8, found: false},
					square{value: 2, found: false},
					square{value: 23, found: false},
					square{value: 4, found: false},
					square{value: 24, found: false},
				},
				group{
					square{value: 21, found: false},
					square{value: 9, found: false},
					square{value: 14, found: false},
					square{value: 16, found: false},
					square{value: 7, found: false},
				},
				group{
					square{value: 6, found: false},
					square{value: 10, found: false},
					square{value: 3, found: false},
					square{value: 18, found: false},
					square{value: 5, found: false},
				},
				group{
					square{value: 1, found: false},
					square{value: 12, found: false},
					square{value: 20, found: false},
					square{value: 15, found: false},
					square{value: 19, found: false},
				},
			},

			args: args{

				input: `22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19
`,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.b.create(tt.args.input)
			if *tt.b != tt.want {
				t.Errorf("board.create() created %v, want %v", *tt.b, tt.want)
			}
		})
	}
}

func Test_group_mark(t *testing.T) {
	type args struct {
		val int
	}
	tests := []struct {
		name string
		g    *group
		args args
		want group
	}{
		{
			name: "Mark One",
			g: &group{
				square{value: 22, found: false},
				square{value: 13, found: false},
				square{value: 17, found: false},
				square{value: 11, found: false},
				square{value: 0, found: false},
			},
			want: group{
				square{value: 22, found: true},
				square{value: 13, found: false},
				square{value: 17, found: false},
				square{value: 11, found: false},
				square{value: 0, found: false},
			},
			args: args{
				val: 22,
			},
		},
		{
			name: "Mark Another",
			g: &group{
				square{value: 22, found: false},
				square{value: 13, found: false},
				square{value: 17, found: false},
				square{value: 11, found: false},
				square{value: 0, found: false},
			},
			want: group{
				square{value: 22, found: false},
				square{value: 13, found: false},
				square{value: 17, found: false},
				square{value: 11, found: false},
				square{value: 0, found: true},
			},
			args: args{
				val: 0,
			},
		},
		{
			name: "Mark None",
			g: &group{
				square{value: 22, found: false},
				square{value: 13, found: false},
				square{value: 17, found: false},
				square{value: 11, found: false},
				square{value: 0, found: false},
			},
			want: group{
				square{value: 22, found: false},
				square{value: 13, found: false},
				square{value: 17, found: false},
				square{value: 11, found: false},
				square{value: 0, found: false},
			},
			args: args{
				val: 5,
			},
		},
		{
			name: "All Already Marked",
			g: &group{
				square{value: 22, found: true},
				square{value: 13, found: true},
				square{value: 17, found: true},
				square{value: 11, found: true},
				square{value: 0, found: true},
			},
			want: group{
				square{value: 22, found: true},
				square{value: 13, found: true},
				square{value: 17, found: true},
				square{value: 11, found: true},
				square{value: 0, found: true},
			},
			args: args{
				val: 5,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.g.mark(tt.args.val)
			if *tt.g != tt.want {
				t.Errorf("group.mark() marked %v, want %v", *tt.g, tt.want)
			}
		})
	}
}

func Test_board_mark(t *testing.T) {
	type args struct {
		val int
	}
	tests := []struct {
		name string
		b    *board
		args args
		want board
	}{
		{
			name: "Mark One",
			b: &board{
				group{
					square{value: 22, found: false},
					square{value: 13, found: false},
					square{value: 17, found: false},
					square{value: 11, found: false},
					square{value: 0, found: false},
				},
				group{
					square{value: 8, found: false},
					square{value: 2, found: false},
					square{value: 23, found: false},
					square{value: 4, found: false},
					square{value: 24, found: false},
				},
				group{
					square{value: 21, found: false},
					square{value: 9, found: false},
					square{value: 14, found: false},
					square{value: 16, found: false},
					square{value: 7, found: false},
				},
				group{
					square{value: 6, found: false},
					square{value: 10, found: false},
					square{value: 3, found: false},
					square{value: 18, found: false},
					square{value: 5, found: false},
				},
				group{
					square{value: 1, found: false},
					square{value: 12, found: false},
					square{value: 20, found: false},
					square{value: 15, found: false},
					square{value: 19, found: false},
				},
			},
			want: board{
				group{
					square{value: 22, found: false},
					square{value: 13, found: false},
					square{value: 17, found: false},
					square{value: 11, found: false},
					square{value: 0, found: false},
				},
				group{
					square{value: 8, found: false},
					square{value: 2, found: false},
					square{value: 23, found: false},
					square{value: 4, found: false},
					square{value: 24, found: false},
				},
				group{
					square{value: 21, found: false},
					square{value: 9, found: false},
					square{value: 14, found: false},
					square{value: 16, found: false},
					square{value: 7, found: false},
				},
				group{
					square{value: 6, found: false},
					square{value: 10, found: false},
					square{value: 3, found: false},
					square{value: 18, found: false},
					square{value: 5, found: false},
				},
				group{
					square{value: 1, found: false},
					square{value: 12, found: true},
					square{value: 20, found: false},
					square{value: 15, found: false},
					square{value: 19, found: false},
				},
			},
			args: args{
				val: 12,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.b.mark(tt.args.val)
			for i, g := range *tt.b {
				for j, s := range g {
					if s != tt.want[i][j] {
						t.Errorf("board.mark() marked %v, want %v", *tt.b, tt.want)
					}
				}
			}
		})
	}
}

func Test_group_bingo(t *testing.T) {
	tests := []struct {
		name string
		g    group
		want bool
	}{
		{
			name: "Nothing is found",
			g: group{
				square{value: 22, found: false},
				square{value: 13, found: false},
				square{value: 17, found: false},
				square{value: 11, found: false},
				square{value: 0, found: false},
			},
			want: false,
		},
		{
			name: "Bingo!",
			g: group{
				square{value: 22, found: true},
				square{value: 13, found: true},
				square{value: 17, found: true},
				square{value: 11, found: true},
				square{value: 0, found: true},
			},
			want: true,
		},
		{
			name: "Two are found",
			g: group{
				square{value: 22, found: false},
				square{value: 13, found: true},
				square{value: 17, found: false},
				square{value: 11, found: true},
				square{value: 0, found: false},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.g.bingo(); got != tt.want {
				t.Errorf("group.bingo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_board_bingo(t *testing.T) {
	tests := []struct {
		name string
		b    board
		want bool
	}{
		{
			name: "No Bingo",
			b: board{
				group{
					square{value: 22, found: false},
					square{value: 13, found: false},
					square{value: 17, found: false},
					square{value: 11, found: false},
					square{value: 0, found: false},
				},
				group{
					square{value: 8, found: false},
					square{value: 2, found: false},
					square{value: 23, found: false},
					square{value: 4, found: false},
					square{value: 24, found: false},
				},
				group{
					square{value: 21, found: false},
					square{value: 9, found: false},
					square{value: 14, found: false},
					square{value: 16, found: false},
					square{value: 7, found: false},
				},
				group{
					square{value: 6, found: false},
					square{value: 10, found: false},
					square{value: 3, found: false},
					square{value: 18, found: false},
					square{value: 5, found: false},
				},
				group{
					square{value: 1, found: false},
					square{value: 12, found: false},
					square{value: 20, found: false},
					square{value: 15, found: false},
					square{value: 19, found: false},
				},
			},
			want: false,
		},
		{
			name: "Row Bingo",
			b: board{
				group{
					square{value: 22, found: false},
					square{value: 13, found: false},
					square{value: 17, found: false},
					square{value: 11, found: false},
					square{value: 0, found: false},
				},
				group{
					square{value: 8, found: true},
					square{value: 2, found: true},
					square{value: 23, found: true},
					square{value: 4, found: true},
					square{value: 24, found: true},
				},
				group{
					square{value: 21, found: false},
					square{value: 9, found: false},
					square{value: 14, found: false},
					square{value: 16, found: false},
					square{value: 7, found: false},
				},
				group{
					square{value: 6, found: false},
					square{value: 10, found: false},
					square{value: 3, found: false},
					square{value: 18, found: false},
					square{value: 5, found: false},
				},
				group{
					square{value: 1, found: false},
					square{value: 12, found: false},
					square{value: 20, found: false},
					square{value: 15, found: false},
					square{value: 19, found: false},
				},
			},
			want: true,
		},
		{
			name: "Column Bingo",
			b: board{
				group{
					square{value: 22, found: false},
					square{value: 13, found: true},
					square{value: 17, found: false},
					square{value: 11, found: false},
					square{value: 0, found: false},
				},
				group{
					square{value: 8, found: false},
					square{value: 2, found: true},
					square{value: 23, found: false},
					square{value: 4, found: false},
					square{value: 24, found: false},
				},
				group{
					square{value: 21, found: false},
					square{value: 9, found: true},
					square{value: 14, found: false},
					square{value: 16, found: false},
					square{value: 7, found: false},
				},
				group{
					square{value: 6, found: false},
					square{value: 10, found: true},
					square{value: 3, found: false},
					square{value: 18, found: false},
					square{value: 5, found: false},
				},
				group{
					square{value: 1, found: false},
					square{value: 12, found: true},
					square{value: 20, found: false},
					square{value: 15, found: false},
					square{value: 19, found: false},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.b.bingo(); got != tt.want {
				t.Errorf("board.bingo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_group_sumUnmarked(t *testing.T) {
	tests := []struct {
		name string
		g    group
		want int
	}{
		{
			name: "Nothing is found",
			g: group{
				square{value: 22, found: false},
				square{value: 13, found: false},
				square{value: 17, found: false},
				square{value: 11, found: false},
				square{value: 0, found: false},
			},
			want: 63,
		},
		{
			name: "All are found",
			g: group{
				square{value: 22, found: true},
				square{value: 13, found: true},
				square{value: 17, found: true},
				square{value: 11, found: true},
				square{value: 0, found: true},
			},
			want: 0,
		},
		{
			name: "Some are found",
			g: group{
				square{value: 22, found: true},
				square{value: 13, found: false},
				square{value: 17, found: false},
				square{value: 11, found: true},
				square{value: 0, found: true},
			},
			want: 30,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.g.sumUnmarked(); got != tt.want {
				t.Errorf("group.sumUnmarked() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_board_sumUnmarked(t *testing.T) {
	tests := []struct {
		name string
		b    board
		want int
	}{
		{
			name: "Column Bingo",
			b: board{
				group{
					square{value: 22, found: false},
					square{value: 13, found: true},
					square{value: 17, found: false},
					square{value: 11, found: false},
					square{value: 0, found: false},
				},
				group{
					square{value: 8, found: false},
					square{value: 2, found: true},
					square{value: 23, found: false},
					square{value: 4, found: false},
					square{value: 24, found: false},
				},
				group{
					square{value: 21, found: false},
					square{value: 9, found: true},
					square{value: 14, found: false},
					square{value: 16, found: false},
					square{value: 7, found: false},
				},
				group{
					square{value: 6, found: false},
					square{value: 10, found: true},
					square{value: 3, found: false},
					square{value: 18, found: false},
					square{value: 5, found: false},
				},
				group{
					square{value: 1, found: false},
					square{value: 12, found: true},
					square{value: 20, found: false},
					square{value: 15, found: false},
					square{value: 19, found: false},
				},
			},
			want: 254,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.b.sumUnmarked(); got != tt.want {
				t.Errorf("board.sumUnmarked() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_board_calculateScore(t *testing.T) {
	type args struct {
		val int
	}
	tests := []struct {
		name string
		b    board
		args args
		want int
	}{
		{
			name: "Column Bingo",
			args: args{
				val: 4,
			},
			b: board{
				group{
					square{value: 22, found: false},
					square{value: 13, found: true},
					square{value: 17, found: false},
					square{value: 11, found: false},
					square{value: 0, found: false},
				},
				group{
					square{value: 8, found: false},
					square{value: 2, found: true},
					square{value: 23, found: false},
					square{value: 4, found: false},
					square{value: 24, found: false},
				},
				group{
					square{value: 21, found: false},
					square{value: 9, found: true},
					square{value: 14, found: false},
					square{value: 16, found: false},
					square{value: 7, found: false},
				},
				group{
					square{value: 6, found: false},
					square{value: 10, found: true},
					square{value: 3, found: false},
					square{value: 18, found: false},
					square{value: 5, found: false},
				},
				group{
					square{value: 1, found: false},
					square{value: 12, found: true},
					square{value: 20, found: true},
					square{value: 15, found: false},
					square{value: 19, found: false},
				},
			},
			want: 936,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.b.calculateScore(tt.args.val); got != tt.want {
				t.Errorf("board.calculateScore() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_board_completeTurn(t *testing.T) {
	type args struct {
		val int
	}
	tests := []struct {
		name string
		b    *board
		args args
		want int
	}{
		{
			name: "Column Bingo",
			args: args{
				val: 12,
			},
			b: &board{
				group{
					square{value: 22, found: false},
					square{value: 13, found: true},
					square{value: 17, found: false},
					square{value: 11, found: false},
					square{value: 0, found: false},
				},
				group{
					square{value: 8, found: false},
					square{value: 2, found: true},
					square{value: 23, found: false},
					square{value: 4, found: false},
					square{value: 24, found: false},
				},
				group{
					square{value: 21, found: false},
					square{value: 9, found: true},
					square{value: 14, found: false},
					square{value: 16, found: false},
					square{value: 7, found: false},
				},
				group{
					square{value: 6, found: false},
					square{value: 10, found: true},
					square{value: 3, found: false},
					square{value: 18, found: false},
					square{value: 5, found: false},
				},
				group{
					square{value: 1, found: false},
					square{value: 12, found: false},
					square{value: 20, found: true},
					square{value: 15, found: false},
					square{value: 19, found: false},
				},
			},
			want: 234 * 12,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.b.completeTurn(tt.args.val); got != tt.want {
				t.Errorf("board.completeTurn() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_playBingo(t *testing.T) {
	type args struct {
		boards []board
		inputs []int
	}
	tests := []struct {
		name      string
		args      args
		wantScore int
	}{
		{
			name:      "Test Provided",
			wantScore: 4512,
			args: args{
				inputs: []int{7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13, 6, 15, 25, 12, 22, 18, 20, 8, 19, 3, 26, 1},
				boards: []board{
					{
						group{
							square{value: 22, found: false},
							square{value: 13, found: false},
							square{value: 17, found: false},
							square{value: 11, found: false},
							square{value: 0, found: false},
						},
						group{
							square{value: 8, found: false},
							square{value: 2, found: false},
							square{value: 23, found: false},
							square{value: 4, found: false},
							square{value: 24, found: false},
						},
						group{
							square{value: 21, found: false},
							square{value: 9, found: false},
							square{value: 14, found: false},
							square{value: 16, found: false},
							square{value: 7, found: false},
						},
						group{
							square{value: 6, found: false},
							square{value: 10, found: false},
							square{value: 3, found: false},
							square{value: 18, found: false},
							square{value: 5, found: false},
						},
						group{
							square{value: 1, found: false},
							square{value: 12, found: false},
							square{value: 20, found: false},
							square{value: 15, found: false},
							square{value: 19, found: false},
						},
					},
					{
						group{
							square{value: 3, found: false},
							square{value: 15, found: false},
							square{value: 0, found: false},
							square{value: 2, found: false},
							square{value: 22, found: false},
						},
						group{
							square{value: 9, found: false},
							square{value: 18, found: false},
							square{value: 13, found: false},
							square{value: 17, found: false},
							square{value: 5, found: false},
						},
						group{
							square{value: 19, found: false},
							square{value: 8, found: false},
							square{value: 7, found: false},
							square{value: 25, found: false},
							square{value: 23, found: false},
						},
						group{
							square{value: 20, found: false},
							square{value: 11, found: false},
							square{value: 10, found: false},
							square{value: 24, found: false},
							square{value: 4, found: false},
						},
						group{
							square{value: 14, found: false},
							square{value: 21, found: false},
							square{value: 16, found: false},
							square{value: 12, found: false},
							square{value: 6, found: false},
						},
					},
					{
						group{
							square{value: 14, found: false},
							square{value: 21, found: false},
							square{value: 17, found: false},
							square{value: 24, found: false},
							square{value: 4, found: false},
						},
						group{
							square{value: 10, found: false},
							square{value: 16, found: false},
							square{value: 15, found: false},
							square{value: 9, found: false},
							square{value: 19, found: false},
						},
						group{
							square{value: 18, found: false},
							square{value: 8, found: false},
							square{value: 23, found: false},
							square{value: 26, found: false},
							square{value: 20, found: false},
						},
						group{
							square{value: 22, found: false},
							square{value: 11, found: false},
							square{value: 13, found: false},
							square{value: 6, found: false},
							square{value: 5, found: false},
						},
						group{
							square{value: 2, found: false},
							square{value: 0, found: false},
							square{value: 12, found: false},
							square{value: 3, found: false},
							square{value: 7, found: false},
						},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotScore := playBingo(tt.args.boards, tt.args.inputs); gotScore != tt.wantScore {
				t.Errorf("playBingo() = %v, want %v", gotScore, tt.wantScore)
			}
		})
	}
}

func Test_loseBingo(t *testing.T) {
	type args struct {
		boards []board
		inputs []int
	}
	tests := []struct {
		name      string
		args      args
		wantScore int
	}{
		{
			name:      "Test Provided",
			wantScore: 1924,
			args: args{
				inputs: []int{7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13, 6, 15, 25, 12, 22, 18, 20, 8, 19, 3, 26, 1},
				boards: []board{
					{
						group{
							square{value: 22, found: false},
							square{value: 13, found: false},
							square{value: 17, found: false},
							square{value: 11, found: false},
							square{value: 0, found: false},
						},
						group{
							square{value: 8, found: false},
							square{value: 2, found: false},
							square{value: 23, found: false},
							square{value: 4, found: false},
							square{value: 24, found: false},
						},
						group{
							square{value: 21, found: false},
							square{value: 9, found: false},
							square{value: 14, found: false},
							square{value: 16, found: false},
							square{value: 7, found: false},
						},
						group{
							square{value: 6, found: false},
							square{value: 10, found: false},
							square{value: 3, found: false},
							square{value: 18, found: false},
							square{value: 5, found: false},
						},
						group{
							square{value: 1, found: false},
							square{value: 12, found: false},
							square{value: 20, found: false},
							square{value: 15, found: false},
							square{value: 19, found: false},
						},
					},
					{
						group{
							square{value: 3, found: false},
							square{value: 15, found: false},
							square{value: 0, found: false},
							square{value: 2, found: false},
							square{value: 22, found: false},
						},
						group{
							square{value: 9, found: false},
							square{value: 18, found: false},
							square{value: 13, found: false},
							square{value: 17, found: false},
							square{value: 5, found: false},
						},
						group{
							square{value: 19, found: false},
							square{value: 8, found: false},
							square{value: 7, found: false},
							square{value: 25, found: false},
							square{value: 23, found: false},
						},
						group{
							square{value: 20, found: false},
							square{value: 11, found: false},
							square{value: 10, found: false},
							square{value: 24, found: false},
							square{value: 4, found: false},
						},
						group{
							square{value: 14, found: false},
							square{value: 21, found: false},
							square{value: 16, found: false},
							square{value: 12, found: false},
							square{value: 6, found: false},
						},
					},
					{
						group{
							square{value: 14, found: false},
							square{value: 21, found: false},
							square{value: 17, found: false},
							square{value: 24, found: false},
							square{value: 4, found: false},
						},
						group{
							square{value: 10, found: false},
							square{value: 16, found: false},
							square{value: 15, found: false},
							square{value: 9, found: false},
							square{value: 19, found: false},
						},
						group{
							square{value: 18, found: false},
							square{value: 8, found: false},
							square{value: 23, found: false},
							square{value: 26, found: false},
							square{value: 20, found: false},
						},
						group{
							square{value: 22, found: false},
							square{value: 11, found: false},
							square{value: 13, found: false},
							square{value: 6, found: false},
							square{value: 5, found: false},
						},
						group{
							square{value: 2, found: false},
							square{value: 0, found: false},
							square{value: 12, found: false},
							square{value: 3, found: false},
							square{value: 7, found: false},
						},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotScore := loseBingo(tt.args.boards, tt.args.inputs); gotScore != tt.wantScore {
				t.Errorf("loseBingo() = %v, want %v", gotScore, tt.wantScore)
			}
		})
	}
}
