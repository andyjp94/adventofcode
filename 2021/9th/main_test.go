package main

import (
	"reflect"
	"testing"
)

func Test_calculateRisk(t *testing.T) {
	type args struct {
		lowPoints []point
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test Provided",
			args: args{
				lowPoints: []point{
					{value: 1},
					{value: 0},
					{value: 5},
					{value: 5},
				},
			},
			want: 15,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculateRisk(tt.args.lowPoints); got != tt.want {
				t.Errorf("calculateRisk() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_heightmap_calculateLowPoints(t *testing.T) {
	tests := []struct {
		name string
		hm   heightmap
		want []point
	}{
		{
			name: "Test Provided",
			hm: heightmap{
				{2, 1, 9, 9, 9, 4, 3, 2, 1, 0},
				{3, 9, 8, 7, 8, 9, 4, 9, 2, 1},
				{9, 8, 5, 6, 7, 8, 9, 8, 9, 2},
				{8, 7, 6, 7, 8, 9, 6, 7, 8, 9},
				{9, 8, 9, 9, 9, 6, 5, 6, 7, 8},
			},
			want: []point{
				{value: 1, coord: coordinate{column: 1, row: 0}},
				{value: 0, coord: coordinate{column: 9, row: 0}},
				{value: 5, coord: coordinate{column: 2, row: 2}},
				{value: 5, coord: coordinate{column: 6, row: 4}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.hm.calculateLowPoints(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("heightmap.calculateLowPoints() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_heightmap_countBasin(t *testing.T) {
	type args struct {
		p point
	}
	tests := []struct {
		name string
		hm   heightmap
		args args
		want map[coordinate]bool
	}{
		{
			name: "Test Provided",
			hm: heightmap{
				{2, 1, 9, 9, 9, 4, 3, 2, 1, 0},
				{3, 9, 8, 7, 8, 9, 4, 9, 2, 1},
				{9, 8, 5, 6, 7, 8, 9, 8, 9, 2},
				{8, 7, 6, 7, 8, 9, 6, 7, 8, 9},
				{9, 8, 9, 9, 9, 6, 5, 6, 7, 8},
			},
			args: args{
				p: point{
					value: 1,
					coord: coordinate{column: 1, row: 0},
				},
			},
			want: map[coordinate]bool{
				{column: 0, row: 0}: true,
				{column: 0, row: 1}: true,
				{column: 1, row: 0}: true,
			},
		},
		{
			name: "0 Test Provided",
			hm: heightmap{
				{2, 1, 9, 9, 9, 4, 3, 2, 1, 0},
				{3, 9, 8, 7, 8, 9, 4, 9, 2, 1},
				{9, 8, 5, 6, 7, 8, 9, 8, 9, 2},
				{8, 7, 6, 7, 8, 9, 6, 7, 8, 9},
				{9, 8, 9, 9, 9, 6, 5, 6, 7, 8},
			},
			args: args{
				p: point{value: 0, coord: coordinate{column: 9, row: 0}},
			},
			want: map[coordinate]bool{
				{column: 9, row: 0}: true,
				{column: 8, row: 0}: true,
				{column: 7, row: 0}: true,
				{column: 6, row: 0}: true,
				{column: 5, row: 0}: true,
				{column: 6, row: 1}: true,
				{column: 8, row: 1}: true,
				{column: 9, row: 2}: true,
			},
		},
		{
			name: "Test Provided - First 5",
			hm: heightmap{
				{2, 1, 9, 9, 9, 4, 3, 2, 1, 0},
				{3, 9, 8, 7, 8, 9, 4, 9, 2, 1},
				{9, 8, 5, 6, 7, 8, 9, 8, 9, 2},
				{8, 7, 6, 7, 8, 9, 6, 7, 8, 9},
				{9, 8, 9, 9, 9, 6, 5, 6, 7, 8},
			},
			args: args{
				p: point{value: 0, coord: coordinate{column: 2, row: 2}},
			},
			want: map[coordinate]bool{
				{column: 2, row: 1}: true,
				{column: 3, row: 1}: true,
				{column: 4, row: 1}: true,
				{column: 1, row: 2}: true,
				{column: 2, row: 2}: true,
				{column: 3, row: 2}: true,
				{column: 4, row: 2}: true,
				{column: 5, row: 2}: true,
				{column: 0, row: 3}: true,
				{column: 1, row: 3}: true,
				{column: 2, row: 3}: true,
				{column: 3, row: 3}: true,
				{column: 4, row: 3}: true,
				{column: 1, row: 4}: true,
			},
		},
		{
			name: "Test Provided - Second 5",
			hm: heightmap{
				{2, 1, 9, 9, 9, 4, 3, 2, 1, 0},
				{3, 9, 8, 7, 8, 9, 4, 9, 2, 1},
				{9, 8, 5, 6, 7, 8, 9, 8, 9, 2},
				{8, 7, 6, 7, 8, 9, 6, 7, 8, 9},
				{9, 8, 9, 9, 9, 6, 5, 6, 7, 8},
			},
			args: args{
				p: point{value: 0, coord: coordinate{column: 6, row: 4}},
			},
			want: map[coordinate]bool{
				{column: 7, row: 2}: true,
				{column: 6, row: 3}: true,
				{column: 7, row: 3}: true,
				{column: 8, row: 3}: true,
				{column: 5, row: 4}: true,
				{column: 6, row: 4}: true,
				{column: 7, row: 4}: true,
				{column: 8, row: 4}: true,
				{column: 9, row: 4}: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.hm.findBasin(tt.args.p, map[coordinate]bool{})
			var failed bool
			for k, _ := range tt.want {
				if _, contains := got[k]; !contains {
					failed = true
					break
				}
			}
			if failed {
				t.Errorf("heightmap.countBasin() = %v, want %v", got, tt.want)

			}
		})
	}
}
