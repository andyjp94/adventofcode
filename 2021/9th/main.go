package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func calculateRisk(lowPoints []point) int {
	var sum int
	for _, v := range lowPoints {
		sum += v.value + 1
	}
	return sum
}

type coordinate struct {
	row, column int
}

func (c coordinate) equals(comp coordinate) bool {
	return c.column == comp.column && c.row == comp.row

}

type point struct {
	value int
	coord coordinate
}

type heightmap [][]int

func (hm heightmap) findBasin(p point, coords map[coordinate]bool) map[coordinate]bool {

	left, right := coordinate{row: p.coord.row, column: p.coord.column - 1}, coordinate{row: p.coord.row, column: p.coord.column + 1}
	up, down := coordinate{row: p.coord.row - 1, column: p.coord.column}, coordinate{row: p.coord.row + 1, column: p.coord.column}

	for _, v := range []coordinate{left, right, up, down} {
		if v.row < 0 || v.row > (len(hm)-1) || v.column < 0 || v.column > (len(hm[0])-1) {
			continue
		}
		newPoint := point{
			value: hm[v.row][v.column],
			coord: coordinate{
				column: v.column,
				row:    v.row,
			},
		}
		if _, contains := coords[p.coord]; !contains {
			coords[p.coord] = true
		}
		if newPoint.value != 9 && newPoint.value > p.value {
			coords[p.coord] = true
			for k := range hm.findBasin(newPoint, coords) {
				coords[k] = true
			}

		}
	}
	return coords
}

func (hm heightmap) calculateBasins(lowPoints []point) int {
	var basins []int
	for _, lp := range lowPoints {
		coords := hm.findBasin(lp, map[coordinate]bool{})
		basins = append(basins, len(coords))
	}
	sort.Ints(basins)
	var sum int = 1
	for _, v := range basins[len(basins)-3:] {
		sum *= v
	}

	return sum
}
func (hm heightmap) calculateLowPoints() []point {
	var lowPoints []point
	for row := 0; row < len(hm); row++ {
		var currentLine []int = hm[row]
		for column := 0; column < len(currentLine); column++ {
			currentValue := currentLine[column]
			var leftValue, rightValue, topValue, bottomValue int = -1, -1, -1, -1
			if column-1 >= 0 {
				leftValue = currentLine[column-1]
			}
			if column+1 < len(currentLine) {
				rightValue = currentLine[column+1]
			}
			if row-1 >= 0 {
				topValue = hm[row-1][column]
			}
			if row+1 < len(hm) {
				bottomValue = hm[row+1][column]
			}

			var lowPointNotFound bool
			for _, v := range []int{leftValue, rightValue, bottomValue, topValue} {
				if v == -1 {
					continue
				}
				if currentValue >= v {
					lowPointNotFound = true
					break
				}
			}
			if !lowPointNotFound {
				lowPoints = append(lowPoints, point{value: currentValue, coord: coordinate{column: column, row: row}})
			}
		}
	}
	return lowPoints
}
func main() {
	contents, err := os.ReadFile("input_data.txt")
	if err != nil {
		panic(err)
	}
	lines := strings.Split(string(contents), "\n")
	var m heightmap = make([][]int, len(lines[0])) // initialize a slice of dy slices
	for i := 0; i < len(lines[0]); i++ {
		m[i] = make([]int, len(lines)) // initialize a slice of dx unit8 in each of dy slices
	}
	for row := 0; row < len(lines); row++ {

		var currentLine string = lines[row]
		for column := 0; column < len(currentLine); column++ {
			m[row][column], err = strconv.Atoi(string(currentLine[column]))
			if err != nil {
				panic(err)
			}

		}
	}
	lowPoints := m.calculateLowPoints()
	fmt.Println(calculateRisk(lowPoints))
	fmt.Println(m.calculateBasins(lowPoints))
}
