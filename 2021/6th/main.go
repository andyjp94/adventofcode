package main

import "fmt"

func simulation(inputs []uint8, days int) (populationSize int) {
	fish := make([]int, 9)
	for _, v := range inputs {
		fish[v] += 1
	}
	for day := 0; day < days; day++ {
		var new_fish = make([]int, 9)
		for i, num := range fish {
			if i == 0 {
				new_fish[6] += num
				new_fish[8] += num
			} else {
				new_fish[i-1] += num
			}
		}
		fish = new_fish
	}
	for _, v := range fish {
		populationSize += v
	}
	return
}
func main() {
	population := []uint8{
		1, 3, 3, 4, 5, 1,
		1, 1, 1, 1, 1, 2,
		1, 4, 1, 1, 1, 5,
		2, 2, 4, 3, 1, 1,
		2, 5, 4, 2, 2, 3,
		1, 2, 3, 2, 1, 1,
		4, 4, 2, 4, 4, 1,
		2, 4, 3, 3, 3, 1,
		1, 3, 4, 5, 2, 5,
		1, 2, 5, 1, 1, 1,
		3, 2, 3, 3, 1, 4,
		1, 1, 4, 1, 4, 1,
		1, 1, 1, 5, 4, 2,
		1, 2, 2, 5, 5, 1,
		1, 1, 1, 2, 1, 1,
		1, 1, 3, 2, 3, 1, 4, 3, 1, 1, 3, 1, 1, 1, 1, 3, 3, 4, 5, 1, 1, 5, 4, 4, 4, 4, 2, 5, 1, 1, 2, 5, 1, 3, 4, 4, 1, 4, 1, 5, 5, 2, 4, 5, 1, 1, 3, 1, 3, 1, 4, 1,
		3, 1, 2, 2, 1, 5, 1, 5, 1, 3, 1, 3, 1, 4, 1, 4, 5, 1, 4, 5, 1, 1, 5, 2, 2, 4, 5, 1, 3, 2, 4, 2, 1, 1, 1, 2, 1, 2, 1, 3, 4, 4, 2, 2, 4, 2, 1, 4, 1, 3, 1, 3,
		5, 3, 1, 1, 2, 2, 1, 5, 2, 1, 1, 1, 1, 1, 5, 4, 3, 5, 3, 3, 1, 5, 5, 4, 4, 2, 1, 1, 1, 2, 5, 3, 3, 2, 1, 1, 1, 5, 5, 3, 1, 4, 4, 2, 4, 2, 1, 1, 1, 5, 1, 2,
		4, 1, 3, 4, 4, 2, 1, 4, 2, 1, 3, 4, 3, 3, 2, 3, 1, 5, 3, 1, 1, 5, 1, 2, 2, 4, 4, 1, 2, 3, 1, 2, 1, 1, 2, 1, 1, 1, 2, 3, 5, 5, 1, 2, 3, 1, 3, 5, 4, 2, 1, 3,
		3, 4}
	max_days := 256
	total_population := simulation(population, max_days)
	fmt.Printf("The size of the fish populations is: %d\n", total_population)

}
