package main

import "testing"

func benchmarkSimulation(days int, b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		simulation([]uint8{
			1, 3, 3, 4, 5, 1,
			1, 1, 1, 1, 1, 2,
			1, 4, 1, 1, 1, 5,
			2, 2, 4, 3, 1, 1,
			2, 5, 4, 2, 2, 3,
			1, 2, 3, 2, 1, 1,
			4, 4, 2, 4, 4, 1,
			2, 4, 3, 3, 3, 1,
			1, 3, 4, 5, 2, 5,
			1, 2, 5, 1, 1, 1,
			3, 2, 3, 3, 1, 4,
			1, 1, 4, 1, 4, 1,
			1, 1, 1, 5, 4, 2,
			1, 2, 2, 5, 5, 1,
			1, 1, 1, 2, 1, 1,
			1, 1, 3, 2, 3, 1, 4, 3, 1, 1, 3, 1, 1, 1, 1, 3, 3, 4, 5, 1, 1, 5, 4, 4, 4, 4, 2, 5, 1, 1, 2, 5, 1, 3, 4, 4, 1, 4, 1, 5, 5, 2, 4, 5, 1, 1, 3, 1, 3, 1, 4, 1,
			3, 1, 2, 2, 1, 5, 1, 5, 1, 3, 1, 3, 1, 4, 1, 4, 5, 1, 4, 5, 1, 1, 5, 2, 2, 4, 5, 1, 3, 2, 4, 2, 1, 1, 1, 2, 1, 2, 1, 3, 4, 4, 2, 2, 4, 2, 1, 4, 1, 3, 1, 3,
			5, 3, 1, 1, 2, 2, 1, 5, 2, 1, 1, 1, 1, 1, 5, 4, 3, 5, 3, 3, 1, 5, 5, 4, 4, 2, 1, 1, 1, 2, 5, 3, 3, 2, 1, 1, 1, 5, 5, 3, 1, 4, 4, 2, 4, 2, 1, 1, 1, 5, 1, 2,
			4, 1, 3, 4, 4, 2, 1, 4, 2, 1, 3, 4, 3, 3, 2, 3, 1, 5, 3, 1, 1, 5, 1, 2, 2, 4, 4, 1, 2, 3, 1, 2, 1, 1, 2, 1, 1, 1, 2, 3, 5, 5, 1, 2, 3, 1, 3, 5, 4, 2, 1, 3,
			3, 4}, days)
	}
}

func BenchmarkSimulation1(b *testing.B) { benchmarkSimulation(1, b) }

func BenchmarkSimulation10(b *testing.B)   { benchmarkSimulation(10, b) }
func BenchmarkSimulation100(b *testing.B)  { benchmarkSimulation(100, b) }
func BenchmarkSimulation1000(b *testing.B) { benchmarkSimulation(1000, b) }
